# ObserveIT on-premises prerequisites check

This document is a small collection of PowerShell and SQL commands to check for ObserveIT prerequisites. The document below describes how the commands should be executed.

## On each Application server and the ObserveIT management console server

```PowerShell
<# Find out who you are currently logged in as. You should be logged in as the
Active Directory Service Account. #>
whoami

<# The following command checks currently installed Windows features.
Nothing very crucial, but good to know. #>
Get-WindowsFeature | ?{$_.InstallState -eq 'Installed'}

<# Test connectivity and access to the screen capture data store.
Populate each variable below with the full path of the Hot Storage, Warm Storage,
and Cold Storage (Archive).
You should not see any errors when running the commands below. #>
$HotStoragePath = ''
$WarmStoragePath = ''
$ColdStoragePath = ''
New-Item -Path "$HotStoragePath" -Name 'test.dat' -ItemType File
Remove-Item -Path "$HotStoragePath"
New-Item -Path "$WarmStoragePath" -Name 'test.dat' -ItemType File
Remove-Item -Path "$WarmStoragePath"
New-Item -Path "$ColdStoragePath" -Name 'test.dat' -ItemType File
Remove-Item -Path "$ColdStoragePath"

<# Determine whether the current machine can connect to the SQL Server
that will host ObserveIT databases.
Populate the SQLServerFQDN variable with the FQDN of the SQL Server. #>
$SQLServerFQDN = ''
Test-NetConnection "$SQLServerFQDN" -Port 1433

<# Find out whether the current user has sysadmin or dbcreator permissions
on the SQL Server.
Source: https://stackoverflow.com/questions/56282469/how-to-check-if-a-sql-server-login-has-role-sysadmin 

Specify the username #>
function QueryDatabaseScript($databaseServer, $dbScript, $timeout)
{
    # Query and return recordset array for passed T-SQL string
    $SQLConnection = New-Object System.Data.SqlClient.SqlConnection
    $SQLConnection.ConnectionString = "server=$($databaseServer);Initial Catalog=master;Integrated Security=True;"

    try
    {        
        $SQLConnection.Open()
        $SQLCommand = New-Object System.Data.SqlClient.SqlCommand
        $SQLCommand.Connection = $SQLConnection 
        $SQLCommand.CommandTimeout = $timeout
        
        $SQLCommand.CommandText = $dbScript
        $reader = $SQLCommand.ExecuteReader()

        $recordsetArray = @()
        while ($reader.Read())
        {
            $recordsetArray += $reader[0]
        }

        $SQLConnection.Close()
        $SQLConnection.Dispose()
    }
    catch
    {
        write-host "QueryDatabaseScript failed.`nError: $($_.Exception.Message)"
        $SQLConnection.Dispose()
        throw $_
    }

    return $recordsetArray
}

$sqlSvr = "$SQLServerFQDN";

$queryResults = QueryDatabaseScript $sqlSvr "select Is_SrvRoleMember('sysadmin')" 0
$isSysadmin = $queryResults[0]
write-host "Current user is a sysadmin: $isSysadmin"

$sqlLogin = 'labjboyko\OITServiceAccount'
$queryResults = QueryDatabaseScript $sqlSvr "select Is_SrvRoleMember('sysadmin', '$sqlLogin')" 0
$isSysadmin = $queryResults[0]
write-host "$sqlLogin is a sysadmin: $isSysadmin"

