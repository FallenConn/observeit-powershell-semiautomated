# Upgrading ObserveIT

This document outlines just the upgrade procedure for the ObserveIT product. This page will keep details to a minimum, and will primarily contain commands used to upgrade ObserveIT. For further details on each command, please review the README document.

**NOTE**: This page assumes the ObserveIT installer .zip file is downloaded to **C:\Temp** on each ObserveIT server, including the Application server, ObserveIT Console, ObserveIT Website Categorization module, and the file server. In this assumption, the full path to the ObserveIT installer is: **C:\Temp\ObserveIT-NL_Setup_v7.9.1.73.zip**.

## Stop network traffic to all ObserveIT Application servers

The command below stops the Microsoft IIS to ensure the traffic to ObserveIT databases stops, and starts the WAS service, to make sure the ObserveIT installer can operate.
Run this command on all ObserveIT Application server and ObserveIT Console servers.

```PowerShell
iisreset /stop
Get-Service WAS | Start-Service
```

## Upgrade ObserveIT databases

Run this command to upgrade the main ObserveIT databases. You can execute the command on any ObserveIT server, including ObserveIT Application server and ObserveIT Console:

```PowerShell
Start-Process "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\DB\SQLPackage.exe"
do {Start-Sleep -Seconds 5} while (!(Test-Path "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\DB\SQL_Setup.txt"))
Get-Content "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\DB\SQL_Setup.txt" -Wait -Last 10

<# Run this command next to upgrade the ObserveIT Analytics database: #>
Start-Process "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\DB_Analytics\SQLPackage.exe"
do {Start-Sleep -Seconds 5} while (!(Test-Path "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\DB_Analytics\SQL_Setup.txt"))
Get-Content "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\DB_Analytics\SQL_Setup.txt" -Wait -Last 10
```

## Upgrade ObserveIT Application servers

Run the commands below on the ObserveIT Application server(s).

Follow the ObserveIT Application server installation wizard.

Select Windows Authentication and enter your password.

When asked to provide username and password, provide credentials for the
ObserveIT Service Account, in the DOMAIN\account format.

```PowerShell
Install-WindowsFeature Web-Server, Web-WebServer, Web-Common-Http, Web-Default-Doc, Web-Dir-Browsing, Web-Http-Errors, Web-Static-Content, Web-Stat-Compression, Web-Security, Web-Filtering, Web-App-Dev, Web-Net-Ext45, Web-Asp, Web-Asp-Net45, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Mgmt-Tools, Web-Mgmt-Console, NET-WCF-Services45, NET-WCF-HTTP-Activation45 –IncludeManagementTools
Start-Process "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\Web\PreRequisite_nodeServices.exe" -ArgumentList "common=1", "/install", "/quiet", "/norestart", "/log PreRequisite_nodeService.log" -Wait
iisreset /start
Start-Process msiexec -ArgumentList '/i', "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\Web\AppServer\ObserveIT.AppServerSetup.msi", '/norestart', '/l*v ObserveITAppServer_setup.txt'
function Set-OITAccount ($Credentials){
    if (!$Credentials) {
        $Credentials = Get-Credential
    }
    $UserName = $Credentials.GetNetworkCredential().Domain + '\' + $Credentials.GetNetworkCredential().UserName
    $Password = $Credentials.GetNetworkCredential().Password

    $OITServices = Get-Service observeit*, screenshot*, websitecat*, gcf1*
    foreach ($Service in $OITServices) {
        $Service = $Service.Name
        Write-Output "Working service $Service"
        $svc_Obj = Get-WmiObject Win32_Service -filter "name='$service'"
        $ChangeStatus = $svc_Obj.change($null, $null, $null, $null, $null,
            $null, $UserName, $Password, $null, $null, $null)
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "User Name sucessfully changed for the service '$Service'"}
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "The service '$Service' Started successfully"}
    }

    foreach ($service in $OITServices) {
        Get-Service $Service.Name | Restart-Service
    }
    Write-Output "Setting credentials for the ObserveIT Application Pool"
    Import-Module WebAdministration
    Get-Item IIS:\AppPools\ObserveIT* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRepository* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRegistry* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Start-Process iisreset -NoNewWindow
}
Set-OITAccount
```

## Upgrade ObserveIT Console

Run the commands below on the ObserveIT Console server(s).

Follow the ObserveIT Console installation wizard.

Select Windows Authentication and enter your password.

When asked to provide username and password, provide credentials for the
ObserveIT Service Account, in the DOMAIN\account format.

```PowerShell
Install-WindowsFeature Web-Server, Web-WebServer, Web-Common-Http, Web-Default-Doc, Web-Dir-Browsing, Web-Http-Errors, Web-Static-Content, Web-Stat-Compression, Web-Security, Web-Filtering, Web-App-Dev, Web-Net-Ext45, Web-Asp, Web-Asp-Net45, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Mgmt-Tools, Web-Mgmt-Console, NET-WCF-Services45, NET-WCF-HTTP-Activation45 –IncludeManagementTools
Start-Process "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\Web\PreRequisite_nodeServices.exe" -ArgumentList "wconly=1", "/install", "/quiet", "/norestart" ,"/log PreRequisite_nodeServices.log" -Wait
Start-Process msiexec -ArgumentList '/i', "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\Web\sqlncli-2012-64-QFE.msi", "/quiet", "/norestart" -Wait
iisreset /start
Start-Process msiexec -ArgumentList '/i', "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\Web\WebConsole\ObserveIT.WebConsoleSetup.msi", '/norestart', 'EXTRACTMICROSERVICES=True', '/l*v ObserveITWebConsole_setup.txt'
function Set-OITAccount ($Credentials){
    if (!$Credentials) {
        $Credentials = Get-Credential
    }
    $UserName = $Credentials.GetNetworkCredential().Domain + '\' + $Credentials.GetNetworkCredential().UserName
    $Password = $Credentials.GetNetworkCredential().Password

    $OITServices = Get-Service observeit*, screenshot*, websitecat*, gcf1*
    foreach ($Service in $OITServices) {
        $Service = $Service.Name
        Write-Output "Working service $Service"
        $svc_Obj = Get-WmiObject Win32_Service -filter "name='$service'"
        $ChangeStatus = $svc_Obj.change($null, $null, $null, $null, $null,
            $null, $UserName, $Password, $null, $null, $null)
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "User Name sucessfully changed for the service '$Service'"}
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "The service '$Service' Started successfully"}
    }

    foreach ($service in $OITServices) {
        Get-Service $Service.Name | Restart-Service
    }
    Write-Output "Setting credentials for the ObserveIT Application Pool"
    Import-Module WebAdministration
    Get-Item IIS:\AppPools\ObserveIT* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRepository* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRegistry* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Start-Process iisreset -NoNewWindow
}
Set-OITAccount
```

## Upgrade ObserveIT Website Categorization module

Run the commands below on the ObserveIT Website Categorization module server(s).

Follow the Website Categorization module installation wizard.

Select Windows Authentication and enter your password.

When asked to provide username and password, provide credentials for the
ObserveIT Service Account, in the DOMAIN\account format.

```PowerShell
Start-Process msiexec -ArgumentList '/i', "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\WebsiteCat\Websitecat_Setup.msi", '/norestart', '/l*v ObserveITWebCat_setup.txt'
do {Start-Sleep -Seconds 5} while (!(Test-Path $env:TEMP\"WebsiteCat_CA_Log.txt"))
Get-Content $env:TEMP\"WebsiteCat_CA_Log.txt" -Wait -Last 10
function Set-OITAccount ($Credentials){
    if (!$Credentials) {
        $Credentials = Get-Credential
    }
    $UserName = $Credentials.GetNetworkCredential().Domain + '\' + $Credentials.GetNetworkCredential().UserName
    $Password = $Credentials.GetNetworkCredential().Password

    $OITServices = Get-Service observeit*, screenshot*, websitecat*, gcf1*
    foreach ($Service in $OITServices) {
        $Service = $Service.Name
        Write-Output "Working service $Service"
        $svc_Obj = Get-WmiObject Win32_Service -filter "name='$service'"
        $ChangeStatus = $svc_Obj.change($null, $null, $null, $null, $null,
            $null, $UserName, $Password, $null, $null, $null)
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "User Name sucessfully changed for the service '$Service'"}
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "The service '$Service' Started successfully"}
    }

    foreach ($service in $OITServices) {
        Get-Service $Service.Name | Restart-Service
    }
    Write-Output "Setting credentials for the ObserveIT Application Pool"
    Import-Module WebAdministration
    Get-Item IIS:\AppPools\ObserveIT* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRepository* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRegistry* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Start-Process iisreset -NoNewWindow
}
Set-OITAccount
```

### Optional: Configure proxy for the Website Categorization module

If the current Website Categorization module deployment requires a connection through a proxy server, use the command below to open the configuration file. Configure the proxy settings under **PROXY_HOST** and **PROXY_PORT**.

```PowerShell
Start-Process notepad.exe -ArgumentList 'C:\Program Files\ObserveIT\WebsiteCat\Adapters\NetStar\db\etc\gcf1.conf' -Wait
function Set-OITAccount ($Credentials){
    if (!$Credentials) {
        $Credentials = Get-Credential
    }
    $UserName = $Credentials.GetNetworkCredential().Domain + '\' + $Credentials.GetNetworkCredential().UserName
    $Password = $Credentials.GetNetworkCredential().Password

    $OITServices = Get-Service observeit*, screenshot*, websitecat*, gcf1*
    foreach ($Service in $OITServices) {
        $Service = $Service.Name
        Write-Output "Working service $Service"
        $svc_Obj = Get-WmiObject Win32_Service -filter "name='$service'"
        $ChangeStatus = $svc_Obj.change($null, $null, $null, $null, $null,
            $null, $UserName, $Password, $null, $null, $null)
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "User Name sucessfully changed for the service '$Service'"}
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "The service '$Service' Started successfully"}
    }

    foreach ($service in $OITServices) {
        Get-Service $Service.Name | Restart-Service
    }
    Write-Output "Setting credentials for the ObserveIT Application Pool"
    Import-Module WebAdministration
    Get-Item IIS:\AppPools\ObserveIT* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRepository* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRegistry* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Start-Process iisreset -NoNewWindow
}
Set-OITAccount
```

## Upgrade the ObserveIT Screenshot Storage Optimizer

The ObserveIT Screenshot Storage Optimizer is usually installed on a file server
or on the ObserveIT Console server.

Log into your ObserveIT Console first, navigate to the **Management Console** >
**Configuration** > **Services Settings** (on your left) > Download the configuration
file for the Screenshot Storage Optimizer.

Follow the Screenshot Storage Optimizer installation wizard.

In the installation wizard, select the configuration you downloaded previously.

Enter the FQDN of your ObserveIT Application server load balancer and
the FQDN of your ObserveIT Console server.

```PowerShell
Start-Process msiexec -ArgumentList '/i', "C:\Temp\ObserveIT-NL_Setup_v7.9.1.73\ScreenshotsStorageOptimizer\ScreenshotsStorageOptimizer.msi", '/norestart', '/l*v ObserveITWebConsole_setup.txt'
function Set-OITAccount ($Credentials){
    if (!$Credentials) {
        $Credentials = Get-Credential
    }
    $UserName = $Credentials.GetNetworkCredential().Domain + '\' + $Credentials.GetNetworkCredential().UserName
    $Password = $Credentials.GetNetworkCredential().Password

    $OITServices = Get-Service observeit*, screenshot*, websitecat*, gcf1*
    foreach ($Service in $OITServices) {
        $Service = $Service.Name
        Write-Output "Working service $Service"
        $svc_Obj = Get-WmiObject Win32_Service -filter "name='$service'"
        $ChangeStatus = $svc_Obj.change($null, $null, $null, $null, $null,
            $null, $UserName, $Password, $null, $null, $null)
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "User Name sucessfully changed for the service '$Service'"}
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "The service '$Service' Started successfully"}
    }

    foreach ($service in $OITServices) {
        Get-Service $Service.Name | Restart-Service
    }
    Write-Output "Setting credentials for the ObserveIT Application Pool"
    Import-Module WebAdministration
    Get-Item IIS:\AppPools\ObserveIT* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRepository* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRegistry* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Start-Process iisreset -NoNewWindow
}
Set-OITAccount
```
