# Testing ObserveIT prerequisites - Application server

Execute the following checks on the Windows Server machines that will how ObserveIT Application server.

## ObserveIT Active Directory Service Account is a member of the local IIS_IUSRS Security Group

```PowerShell
Get-LocalGroup IIS_IUSRS | Get-LocalGroupMember
```

Expect a result similar to this:

|ObjectClass|Name                        |PrincipalSource|
|-----------|----------------------------|---------------|
|User       |labjboyko\OITServiceAccount |ActiveDirectory|


## ObserveIT Active Directory Service Account is a member of the local administrators Security Group

```PowerShell
Get-LocalGroup Administrators | Get-LocalGroupMember
```

Expect a result similar to this:

|ObjectClass|Name                        |PrincipalSource|
|-----------|----------------------------|---------------|
|User       |labjboyko\OITServiceAccount |ActiveDirectory|

## Check if all the Windows prerequisites are installed

```PowerShell
Get-WindowsFeature Web-Server, Web-WebServer, Web-Common-Http, Web-Default-Doc, Web-Dir-Browsing, Web-Http-Errors, Web-Static-Content, Web-Stat-Compression, Web-Security, Web-Filtering, Web-App-Dev, Web-Net-Ext45, Web-Asp, Web-Asp-Net45, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Mgmt-Tools, Web-Mgmt-Console, NET-WCF-Services45, NET-WCF-HTTP-Activation45 | Where-Object {$_.InstallState -ne 'Installed'}
```

Expect any feature that is not currently installed to appear in a list. If the command returns no output - all the prerequisites are installed.

## Test connectivity to the SQL Server

```PowerShell
Test-NetConnection <# hostname of the SQL Server #> -Port 1433
```

Expect TCPConnect result to be True.

## Test connectivity to the screen capture data store

```PowerShell
Test-NetConnection <# hostname of the screen capture data store #> -Port 445
```

Expect TCPConnect result to be True.
