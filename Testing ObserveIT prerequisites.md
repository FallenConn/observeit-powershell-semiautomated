# Testing ObserveIT prerequisites

The text in this section describes steps to check prerequisites for deployment or upgrade of the ObserveIT software. Nearly every step contains a command or a set of commands to validate the prerequisite, and the expected result.

Unless otherwise specified, the commands below are Microsoft PowerShell commands. Log in using the ObserveIT Active Directory Service Account and run PowerShell as administrator on the current machine. And paste the commands into the PowerShell window.