# Sample installation commands and sequences

This page contains sample commands and sequences to install and upgrade ObserveIT silently. The page is primarily focused on the ObserveIT back-end. This is intended to help you make your own PowerShell-based automation for installing or upgrading ObserveIT. 

>**NOTE**
>
>This page was updated to support the changes in
>the prerequisites installer for ObserveIT v7.11.0.

If you are managing ObserveIT and are responsible for ObserveIT deployment in your organization, you should be familiar with [ObserveIT installation procedure](https://documentation.observeit.com/installation_guide/custom_installation.htm) described in the [ObserveIT documentation](https://documentation.observeit.com/).

## Intended audience

This document is installed for ObserveIT administrators or IT teams that have above basic understanding of PowerShell and/or automation and ochestration, and that would like to automate or semi-automate ObserveIT backned installation and upgrade.

## Notes

- Due to its nature and performance requirements, most often ObserveIT is deployed in a Custom Installation mode.
- The commands below describe installing ObserveIT using an Active Directory Service Account.
- Please do note that fully automating ObserveIT upgrade is not currently possible, as upgrading the databases requires upgrade activation key.
- The ObserveIT Screenshot Storage Optimizer component must be installed manually as well, starting ObserveIT version v7.9. This is due to a requirement to download a configuration file using the ObserveIT Console.

## Product overview

ObserveIT back-end contains the following components:

- Database: An Microsoft SQL Server engine that stores ObserveIT databases. This component is the central piece of ObserveIT, as it contains the textual metadata collected, as well as the product configuration.
- ObserveIT Application server: This is a scale-able ingestion point for ObserveIT Agents. The data collected from ObserveIT Agents is received and processed here.
- ObserveIT Website Categorization module: An engine that categorizes websites. If required by a condition, ObserveIT Application servers reach out to the Website Categorization module to identify the category of a website visited.
- ObserveIT Console: This is your main interaction interface with ObserveIT. In addition, the ObserveIT RestAPI is installed on this server.
- ObserveIT Agent: These are Agents deployed on Windows, macOS, Unix, and Linux machines.

## Installation steps overview

Below is an overview of steps when installing ObserveIT. This describes only installing ObserveIT components, and does not describe [prerequisites](https://documentation.observeit.com/installation_guide/ci_custom_installation_prerequisites.htm). You can look at full overview of installation steps [here](https://documentation.observeit.com/installation_guide/ci_custom_installation_steps.htm).

Installation steps:

1. Install ObserveIT databases.
2. Install ObserveIT Console prerequisites.
3. Install the ObserveIT Console.
4. Install Application server prerequisites.
5. Install ObserveIT Application server or Application servers for larger environments.
6. Install the Website Categorization module.
7. Install the ObserveIT Screenshot Storage Optimizer.

## Installation Deployment commands

Commands below walk you through installing ObserveIT back-end. Watch for in-text comments that describe the steps.

There are two ways of installing ObserveIT - using an Active Directory Service Account, or using a local system account. The Service Account route is the preferred route - use the latter route if you are installing ObserveIT on a single machine, or if you are capturing metadata-only, without capturing screen capture data.

### Installing ObserveIT databases

```PowerShell
<# We are installing databases first.
You can install the databases from any server you intend for ObserveIT components, such as
the ObserveIT Console, ObserveIT Application server, or ObserveIT Website Categorization module.
All the roles mentioned above need access to the SQL Server #>

# First, define the installer path:
$observeitInstallerPath = "<Enter full path to the root of the new ObserveIT installer here>"
<# Specifically, point to the path where all the extracted ObserveIT installer folders are, like 
DB, DB_Analytics, Web, and others. #>

<# Now we can call the database installer.
Make sure the server has access to the database server #>
Start-Process "$observeitInstallerPath\DB\SQLPackage.exe" -ArgumentList "/server:<FQDN of the SQL Server>", "/makedatabase", "/quiet" -Wait
# Look through the output log to validate the installer ran successfully
Get-Content "$observeitInstallerPath\DB\Sql_Setup.txt" | Select-String -Pattern "Package executed successfully"

<# Now run installer for the Analytics database #>
Start-Process "$observeitInstallerPath\DB_Analytics\SQLPackage.exe" -ArgumentList "/server:<FQDN of the SQL Server>", "/makedatabase", "/quiet" -Wait
<# ...and validate that installation completed successfully as well #>
Get-Content "$observeitInstallerPath\DB_Analytics\Sql_Setup.txt" | Select-String -Pattern "Package executed successfully"
<# This concludes installation of ObserveIT databases. #>
```

### Installing ObserveIT Console

```PowerShell
<# Let's define  variables to be used during the ObserveIT Console installation
NOTE: Make sure all the commands below are executed in the context
of the ObserveIT Active Directory Service Account #>
$observeitInstallerPath = "<Enter full path to the root of the new ObserveIT installer here>"
$SQLServer = '<FQDN of the SQL Server>'                                     # FQDN of the SQL Server for ObserveIT.
$DNSForestName = "<enter your DNS domain name here>"                        # For example, contoso.local
$Creds = Get-Credential -UserName "<Enter ObserveIT Service Account username here in the DOMAIN\account format>" -Message "Enter ObserveIT Service Account credentials"
$WebSiteName = 'ObserveITWebConsole'                                        # Name of the website for the ObserveIT Console. ObserveITWebConsole is the preferred default.
$WebSitePort = "443"                                                        # Port for the ObserveIT Console website. 443 is the preferred default.
$WebSiteProtocol = "https"                                                  # Protocol for the ObserveIT Console website. https is the preferred default.
$ComputerName = (Get-WmiObject -Class Win32_ComputerSystem).PSComputerName  # This gathers the computer name for further use.
$MachineFQDN = $ComputerName + '.' + $DNSForestName                         # This generates full machine FQDN.
$ApplicationPool = "IIS:\AppPools\$WebSiteName"                             # Generates the IIS Application Pool name.
$WebSiteBinding = ":" + $WebSitePort + ":"                                  # Generates the IIS binding string.
$OutputDestination = 'C:\temp'                                              # Define a destination for the certificate export later on.

<# Validate the credentials are correct.
If you get no error back - the credentials are correct.
If an error comes back - the credentials are incorrect. #>
Start-Process cmd.exe -Credential $creds -NoNewWindow -ArgumentList "/c" -Wait

<# Make sure the ObserveIT installer files are unblocked #>
Get-ChildItem $observeitInstallerPath -Recurse | Foreach-Object {Unblock-File $_.Fullname}

<# Here, we prepare for the ObserveIT Console. #>
<# Install Microsoft IIS and required components #>
Install-WindowsFeature Web-Server, Web-WebServer, Web-Common-Http, Web-Default-Doc, Web-Dir-Browsing, Web-Http-Errors, Web-Static-Content, Web-Stat-Compression, Web-Security, Web-Filtering, Web-App-Dev, Web-Net-Ext45, Web-Asp, Web-Asp-Net45, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Mgmt-Tools, Web-Mgmt-Compat, Web-Mgmt-Console, NET-WCF-Services45, NET-WCF-HTTP-Activation45 –IncludeManagementTools

<# Next, create the folder structure for the IIS website #>
New-Item -Name ObserveIT -Path "C:\Program Files" -ItemType Directory
New-Item -Name Web -Path "C:\Program Files\ObserveIT" -ItemType Directory

<# Create the IIS Application Pool and website #>
Import-Module WebAdministration
New-Item $ApplicationPool -Force
New-Item IIS:\Sites\$WebSiteName -PhysicalPath 'C:\Program Files\ObserveIT\Web\' -Bindings @{protocol = $WebSiteProtocol; bindingInformation = $WebSiteBinding } -Force
Set-ItemProperty IIS:\Sites\$WebSiteName\ -Name applicationpool -Value $WebSiteName -Force
if ((Get-Item 'IIS:\Sites\Default Web Site')) { Remove-Item 'IIS:\Sites\Default Web Site'; Write-Output "Removing default website" } else { Write-Output "Default website is already gone. Continuing." }
if ((Get-Item 'IIS:\AppPools\DefaultAppPool')) { Remove-Item 'IIS:\AppPools\DefaultAppPool'; Write-Output "Removing DefaultAppPool" } else { Write-Output "DefaultAppPool is already gone. Continuing." }

<# Disable IIS logging #>
Set-ItemProperty -Path "IIS:\Sites\$WebSiteName" -Name Logfile.enabled -Value $false

<# Configure the Application Pool to recycle during times of less load #>
Import-Module WebAdministration
# Set the Application Pool to recycle every 8 hours
Set-ItemProperty $ApplicationPool -Name Recycling.periodicRestart.time -Value 0.08:00:00
Clear-ItemProperty $ApplicationPool -Name Recycling.periodicRestart.schedule
$RestartAt = @('12:00', '20:00', '07:00')
# Set recycling to occur at 12pm, 8pm, and 7am, when there's less load on the system
New-ItemProperty -Path $ApplicationPool -Name Recycling.periodicRestart.schedule -Value $RestartAt

<# Require TLS for the ObserveIT management console website.
If you have IIS Management Console open - please close it before running this command. #>
$ConfigSection = Get-IISConfigSection -SectionPath "system.webServer/security/access" -Location "$WebSiteName"
Set-IISConfigAttributeValue -AttributeName sslFlags -AttributeValue Ssl -ConfigElement $ConfigSection
Get-IISConfigAttributeValue -ConfigElement $ConfigSection -AttributeName sslFlags

<# Next, we will create and assign an self-signed certificate for this website. You can either 
provision your own and skip this section, or continue and change the certificate later. #>
$SelfSignedCertificate = New-SelfSignedCertificate -DnsName $MachineFQDN, $ComputerName -CertStoreLocation cert:\LocalMachine\My -FriendlyName $MachineFQDN -KeyExportPolicy NonExportable -NotAfter (Get-Date).AddYears(5)

<# Now, we will being the certificate to the website #>
$binding = Get-WebBinding -Name $WebSiteName -Protocol $WebSiteProtocol
$binding.AddSslCertificate($SelfSignedCertificate.GetCertHashString(), 'my')

<# Lastly, export the self-signed certificate so you can import
the certificate into other servers or machines. #>
Export-Certificate -Cert $SelfSignedCertificate -FilePath "$OutputDestination\$MachineFQDN.crt" -Force

<# With the IIS components installed, and configured, we can install ObserveIT prerequisites packages.
First, we are installing the NodeJS and .Net Core. #>
$NodeJSInstaller = $observeitInstallerPath + '\Web\PreRequisite_nodeServices.exe'
$ComponentInstallArguments = "wconly=1", "sqlcli=1", "/install", "/quiet", "/norestart" ,"/log PreRequisite_nodeServices.log"
Start-Process $NodeJSInstaller -ArgumentList $ComponentInstallArguments -Wait

<# Before installing the application itself, let's make sure the Service Account has
the necessary permissions to run as a service. #>
function Set-LogonRight ($accountToAdd) {
    if ( [string]::IsNullOrEmpty($accountToAdd) ) {
        Write-Output "no account specified"
        exit
}
    $sidstr = $null
try {
        $ntprincipal = new-object System.Security.Principal.NTAccount "$accountToAdd"
        $sid = $ntprincipal.Translate([System.Security.Principal.SecurityIdentifier])
        $sidstr = $sid.Value.ToString()
} catch {
        $sidstr = $null
}
    Write-Output "Account: $($accountToAdd)"
    if ( [string]::IsNullOrEmpty($sidstr) ) {
        Write-Output "Account not found!"
}
    Write-Output "Account SID: $($sidstr)"
    $tmp = [System.IO.Path]::GetTempFileName()
    Write-Output "Export current Local Security Policy"
    secedit.exe /export /cfg "$($tmp)" 
    $c = Get-Content -Path $tmp 
    $currentSetting = ""
    foreach ($s in $c) {
        if ( $s -like "SeServiceLogonRight*") {
            $x = $s.split("=", [System.StringSplitOptions]::RemoveEmptyEntries)
            $currentSetting = $x[1].Trim()
    }
}
    if ( $currentSetting -notlike "*$($sidstr)*" ) {
        Write-Output "Modify Setting ""Logon as a Service"""
        if ( [string]::IsNullOrEmpty($currentSetting) ) {
            $currentSetting = "*$($sidstr)"
    } else {
            $currentSetting = "*$($sidstr),$($currentSetting)"
    }
        Write-Output "$currentSetting"
$outfile = @"
[Unicode]
Unicode=yes
[Version]
signature="`$CHICAGO`$"
Revision=1
[Privilege Rights]
SeServiceLogonRight = $($currentSetting)
"@
        $tmp2 = [System.IO.Path]::GetTempFileName()
        Write-Output "Import new settings to Local Security Policy"
        $outfile | Set-Content -Path $tmp2 -Encoding Unicode -Force
        Push-Location (Split-Path $tmp2)
    try {
            secedit.exe /configure /db "secedit.sdb" /cfg "$($tmp2)" /areas USER_RIGHTS 
    } finally { 
            Pop-Location
    }
} else {
        Write-Output "NO ACTIONS REQUIRED! Account already in ""Logon as a Service"""
}
    Write-Output "Done."
}

Set-LogonRight "$($Creds.GetNetworkCredential().UserName)"

<# Let's start the WAS service since it's required for the installation #>
Get-Service WAS | Start-Service

<# Let's confirm DefaultAppPool exists. If not - let's create it. #>
if (!(Get-WebAppPool -Name DefaultAppPool)) { New-WebAppPool -Name DefaultAppPool; Write-Output "Adding DefaultAppPool" } else { Write-Output "DefaultAppPool exists. Continuing." }

<# Now we can install the ObserveIT Console itself. This will install the ObserveIT Console
and the Advanced ObserveIT Console components. #>
$ComponentInstallArguments = "/i", ($observeitInstallerPath + '\Web\WebConsole\ObserveIT.WebConsoleSetup.msi'), "/qb", "/norestart", "DATABASE_SERVER=$SQLServer", "TARGETAPPPOOL=$WebSiteName", "TARGETSITE=$WebSiteName", "DATABASE_LOGON_TYPE=WindowsAccount", "SERVICE_USERNAME=$($Creds.GetNetworkCredential().Domain + '\' + $Creds.GetNetworkCredential().UserName)", "SERVICE_PASSWORD=$($Creds.GetNetworkCredential().Password)","/leo", ".\WebConsoleMSI.log"
Start-Process msiexec.exe -ArgumentList $ComponentInstallArguments -Wait -NoNewWindow

<# Validate ObserveIT Web Management Console was installed correctly. #>
if ((Get-Content $env:LOCALAPPDATA\temp\WebConsole_CA_Log.txt | Select-String Done -CaseSensitive | Select-Object -Last 10 | Measure-Object).Count -eq 10) { Write-Output "ObserveIT Web Management Console installation succeeded." } else { Write-Output "ObserveIT Web Management Console installation might not have succeeded, please examine trace logs."}

<# Extract the Advanced ObserveIT Web Management Console #>
Set-Location "C:\Program Files\ObserveIT\Web\V2\apis"
Get-ChildItem *.zip | foreach {Expand-Archive -LiteralPath $_.FullName -DestinationPath $($_.Directory.ToString() + '\' + $_.BaseName.ToString()) -Force}
Set-Location "C:\Program Files\ObserveIT\Web\V2\apps"
Get-ChildItem *.zip | foreach {Expand-Archive -LiteralPath $_.FullName -DestinationPath $($_.Directory.ToString() + '\' + $_.BaseName.ToString()) -Force}

<# Now, let's validate the installation succeeded.
For that, we will make sure PowerShell ignores TLS trust (so we can execute the command
against localhost) and will then make sure the ObserveIT Console login page loads. #>
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
iisreset /start
Import-Module WebAdministration
Get-IISSite $WebSiteName | Start-IISSite
$CurrentURL = $WebSiteProtocol + '://localhost:' + $WebSitePort + '/ObserveIT/FormLoginAuth.aspx?UserDefaultPage=True'
(Invoke-WebRequest $CurrentURL).RawContent -match 'ObserveIT - Login Page'
<# Expect to receive True as a success response. #>

<# This concludes installation of the ObserveIT Console. #>
```

#### Update default policies to preferred default values and ObserveIT management console URL

Use SQL Management Studio to execute these queries:

```SQL
UPDATE ObserveIT.dbo.GroupPolicyConfigurationItem SET PropertyValue = 1 WHERE (PropertyId = 63 AND GroupPolicyId = '00000000-0000-0000-0000-000000000000');
UPDATE ObserveIT.dbo.GroupPolicyConfigurationItem SET PropertyValue = 1 WHERE (PropertyId = 119 AND GroupPolicyId = '00000000-0000-0000-0000-000000000000');
UPDATE ObserveIT.dbo.GroupPolicyConfigurationItem SET PropertyValue = 1 WHERE (PropertyId = 139 AND GroupPolicyId = '00000000-0000-0000-0000-000000000000');
UPDATE ObserveIT.dbo.GroupPolicyConfigurationItem SET PropertyValue = 5000 WHERE (PropertyId = 25 AND GroupPolicyId = '00000000-0000-0000-0000-000000000000');
UPDATE ObserveIT.dbo.WebConsoles SET WebConsoleFqdnUrl = '<# http/https://<# FQDN of the ObserveIT management console server #>:<# port #>/ObserveIT #>';
```

Restart IIS now.

### Installing ObserveIT Application server

```PowerShell
<# The procedure of installing ObserveIT Application server is very similar to the ObserveIT Console.
Nearly the same, in fact, except the website and Application Pools names
NOTE: Make sure all the commands below are executed in the context
of the ObserveIT Active Directory Service Account #>

<# Let's define  variables to be used during the ObserveIT Application server installation #>
$observeitInstallerPath = "<Enter full path to the root of the new ObserveIT installer here>"
$SQLServer = '<FQDN of the SQL Server>'                                     # FQDN of the SQL Server for ObserveIT
$DNSForestName = "<enter your DNS domain name here>"                        # For example, contoso.local
$Creds = Get-Credential -UserName "<Enter ObserveIT Service Account username here in the DOMAIN\account format>" -Message "Enter ObserveIT Service Account credentials"
$WebSiteName = 'ObserveITApplicationServer'                                 # Name of the website for the ObserveIT Console. ObserveITWebConsole is the preferred default.
$WebSitePort = "443"                                                        # Port for the ObserveIT Console website. 443 is the preferred default.
$WebSiteProtocol = "https"                                                  # Protocol for the ObserveIT Console website. https is the preferred default.
$ComputerName = (Get-WmiObject -Class Win32_ComputerSystem).PSComputerName  # This gathers the computer name for further use.
$MachineFQDN = $ComputerName + '.' + $DNSForestName                         # This generates full machine FQDN.
$ApplicationPool = "IIS:\AppPools\$WebSiteName"                             # Generates the IIS Application Pool name.
$WebSiteBinding = ":" + $WebSitePort + ":"                                  # Generates the IIS binding string.
$OutputDestination = 'C:\temp'                                              # Define a destination for the certificate export later on.

<# Validate the credentials are correct.
If you get no error back - the credentials are correct.
If an error comes back - the credentials are incorrect. #>
Start-Process cmd.exe -Credential $creds -NoNewWindow -ArgumentList "/c" -Wait

<# Make sure the ObserveIT installer files are unblocked #>
Get-ChildItem $observeitInstallerPath -Recurse | Foreach-Object {Unblock-File $_.Fullname}

<# Here, we prepare for the ObserveIT Console. #>
<# Install Microsoft IIS and required components #>
Install-WindowsFeature Web-Server, Web-WebServer, Web-Common-Http, Web-Default-Doc, Web-Dir-Browsing, Web-Http-Errors, Web-Static-Content, Web-Stat-Compression, Web-Security, Web-Filtering, Web-App-Dev, Web-Net-Ext45, Web-Asp, Web-Asp-Net45, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Mgmt-Tools, Web-Mgmt-Compat, Web-Mgmt-Console, NET-WCF-Services45, NET-WCF-HTTP-Activation45 –IncludeManagementTools

<# Next, create the folder structure for the IIS website #>
New-Item -Name ObserveIT -Path "C:\Program Files" -ItemType Directory
New-Item -Name Web -Path "C:\Program Files\ObserveIT" -ItemType Directory

<# Create the IIS Application Pool and website #>
Import-Module WebAdministration
New-Item $ApplicationPool -Force
New-Item IIS:\Sites\$WebSiteName -PhysicalPath 'C:\Program Files\ObserveIT\Web\' -Bindings @{protocol = $WebSiteProtocol; bindingInformation = $WebSiteBinding } -Force
Set-ItemProperty IIS:\Sites\$WebSiteName\ -Name applicationpool -Value $WebSiteName -Force
if ((Get-Item 'IIS:\Sites\Default Web Site')) { Remove-Item 'IIS:\Sites\Default Web Site'; Write-Output "Removing default website" } else { Write-Output "Default website is already gone. Continuing." }
if ((Get-Item 'IIS:\AppPools\DefaultAppPool')) { Remove-Item 'IIS:\AppPools\DefaultAppPool'; Write-Output "Removing DefaultAppPool" } else { Write-Output "DefaultAppPool is already gone. Continuing." }

<# Disable IIS logging #>
Set-ItemProperty -Path "IIS:\Sites\$WebSiteName" -Name Logfile.enabled -Value $false

<# Configure the Application Pool to recycle during times of less load #>
Import-Module WebAdministration
# Set the Application Pool to recycle every 8 hours
Set-ItemProperty $ApplicationPool -Name Recycling.periodicRestart.time -Value 0.08:00:00
Clear-ItemProperty $ApplicationPool -Name Recycling.periodicRestart.schedule
$RestartAt = @('12:00', '20:00', '07:00')
# Set recycling to occur at 12pm, 8pm, and 7am, when there's less load on the system
New-ItemProperty -Path $ApplicationPool -Name Recycling.periodicRestart.schedule -Value $RestartAt

<# Require TLS for the Application server website.
If you have IIS Management Console open - please close it before running this command. #>
$ConfigSection = Get-IISConfigSection -SectionPath "system.webServer/security/access" -Location "$WebSiteName"
Set-IISConfigAttributeValue -AttributeName sslFlags -AttributeValue Ssl -ConfigElement $ConfigSection
Get-IISConfigAttributeValue -ConfigElement $ConfigSection -AttributeName sslFlags

<# Next, we will create and assign an self-signed certificate for this website. You can either 
provision your own and skip this section, or continue and change the certificate later. #>
$SelfSignedCertificate = New-SelfSignedCertificate -DnsName $MachineFQDN, $ComputerName -CertStoreLocation cert:\LocalMachine\My -FriendlyName $MachineFQDN -KeyExportPolicy NonExportable -NotAfter (Get-Date).AddYears(5)

<# Now, we will being the certificate to the website #>
$binding = Get-WebBinding -Name $WebSiteName -Protocol $WebSiteProtocol
$binding.AddSslCertificate($SelfSignedCertificate.GetCertHashString(), 'my')

<# Lastly, export the self-signed certificate so you can import
the certificate into other servers or machines. #>
Export-Certificate -Cert $SelfSignedCertificate -FilePath "$OutputDestination\$MachineFQDN.crt" -Force

<# With the IIS components installed, and configured, we can install ObserveIT prerequisites packages.
Let's install the NodeJS and .Net Core. #>
$NodeJSInstaller = $observeitInstallerPath + '\Web\PreRequisite_nodeServices.exe'
$ComponentInstallArguments = "common=1", "/install", "/quiet", "/norestart", "/log PreRequisite_nodeService.log"
Start-Process $NodeJSInstaller -ArgumentList $ComponentInstallArguments -Wait

<# Before installing the application itself, let's make sure the Service Account has
the necessary permissions to run as a service. #>
function Set-LogonRight ($accountToAdd) {
    if ( [string]::IsNullOrEmpty($accountToAdd) ) {
        Write-Output "no account specified"
        exit
}
    $sidstr = $null
try {
        $ntprincipal = new-object System.Security.Principal.NTAccount "$accountToAdd"
        $sid = $ntprincipal.Translate([System.Security.Principal.SecurityIdentifier])
        $sidstr = $sid.Value.ToString()
} catch {
        $sidstr = $null
}
    Write-Output "Account: $($accountToAdd)"
    if ( [string]::IsNullOrEmpty($sidstr) ) {
        Write-Output "Account not found!"
        exit -1
}
    Write-Output "Account SID: $($sidstr)"
    $tmp = [System.IO.Path]::GetTempFileName()
    Write-Output "Export current Local Security Policy"
    secedit.exe /export /cfg "$($tmp)" 
    $c = Get-Content -Path $tmp 
    $currentSetting = ""
    foreach ($s in $c) {
        if ( $s -like "SeServiceLogonRight*") {
            $x = $s.split("=", [System.StringSplitOptions]::RemoveEmptyEntries)
            $currentSetting = $x[1].Trim()
    }
}
    if ( $currentSetting -notlike "*$($sidstr)*" ) {
        Write-Output "Modify Setting ""Logon as a Service"""
        if ( [string]::IsNullOrEmpty($currentSetting) ) {
            $currentSetting = "*$($sidstr)"
    } else {
            $currentSetting = "*$($sidstr),$($currentSetting)"
    }
        Write-Output "$currentSetting"
$outfile = @"
[Unicode]
Unicode=yes
[Version]
signature="`$CHICAGO`$"
Revision=1
[Privilege Rights]
SeServiceLogonRight = $($currentSetting)
"@
        $tmp2 = [System.IO.Path]::GetTempFileName()
        Write-Output "Import new settings to Local Security Policy"
        $outfile | Set-Content -Path $tmp2 -Encoding Unicode -Force
        Push-Location (Split-Path $tmp2)
    try {
            secedit.exe /configure /db "secedit.sdb" /cfg "$($tmp2)" /areas USER_RIGHTS 
    } finally { 
            Pop-Location
    }
} else {
        Write-Output "NO ACTIONS REQUIRED! Account already in ""Logon as a Service"""
}
    Write-Output "Done."
}

Set-LogonRight "$($Creds.GetNetworkCredential().UserName)"

<# Let's start the WAS service since it's required for the installation #>
Get-Service WAS | Start-Service

<# Let's confirm DefaultAppPool exists. If not - let's create it. #>
if (!(Get-WebAppPool -Name DefaultAppPool)) { New-WebAppPool -Name DefaultAppPool; Write-Output "Adding DefaultAppPool" } else { Write-Output "DefaultAppPool exists. Continuing." }

<# Now we can install the ObserveIT Application server itself.#>
$ComponentInstallArguments = "/i", ($observeitInstallerPath + '\Web\AppServer\ObserveIT.AppServerSetup.msi'), "/qb", "/norestart", "DATABASE_SERVER=$SQLServer", "TARGETAPPPOOL=$WebSiteName", "TARGETSITE=$WebSiteName", "DATABASE_LOGON_TYPE=WindowsAccount", "SERVICE_USERNAME=$($Creds.GetNetworkCredential().Domain + '\' + $Creds.GetNetworkCredential().UserName)", "SERVICE_PASSWORD=$($Creds.GetNetworkCredential().Password)","/leo", ".\AppServerMSI.log"
Start-Process msiexec.exe -ArgumentList $ComponentInstallArguments -Wait -NoNewWindow

<# Validate ObserveIT Application Server was installed correctly. #>
if ((Get-Content $env:LOCALAPPDATA\temp\AppServer_CA_Log.txt | Select-String Done -CaseSensitive | Select-Object -Last 3 | Measure-Object).Count -eq 3) { Write-Output "ObserveIT Application Server installation succeeded." } else { Write-Output "ObserveIT Application Server installation might not have succeeded, please examine trace logs." }

<# Now, let's validate the installation succeeded.
For that, we will make sure PowerShell ignores TLS trust (so we can execute the command
against localhost) and will then make sure ObserveIT Application server health check is successful. #>
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
iisreset /start
Import-Module WebAdministration
Get-IISSite $WebSiteName | Start-IISSite
$CurrentURL = $WebSiteProtocol + '://localhost:' + $WebSitePort + '/ObserveitApplicationServer/v2/apis/health/_health'
Invoke-WebRequest $CurrentURL -UseBasicParsing
<# You should get code 200 as a response. #>
```

#### Update Application server URLs in the database

```SQL
-- Update the load balancer URL
UPDATE ObserveIT.dbo.DatabaseConfiguration
SET DatabaseConfigurationValue = '<# http/https #>://<FQDN>/<# port #>/ObserveITApplicationServer'
WHERE DatabaseConfigurationKey = 'AppServerUrl'
GO

-- Update addresses of each specific Application server
UPDATE ObserveIT.dbo.ApplicationServers
SET ApplicationServerUrl = '<# http/https #>://<FQDN of load balancer>/<# port #>/ObserveITApplicationServer'
WHERE ServerName = '<# hostname of this Application server #>'
GO

UPDATE ObserveIT.dbo.ApplicationServers
SET ApplicationServerInternalUrl = '<# http/https #>://<FQDN of this Application server>/<# port #>/ObserveITApplicationServer'
WHERE ServerName = '<# hostname of this Application server #>'
GO
```

Restart IIS now. This concludes installation of the ObserveIT Application server.

### Installing ObserveIT Website Categorization module

```PowerShell
<# Let's define  variables to be used during the ObserveIT Application server installation
NOTE: Make sure all the commands below are executed in the context
of the ObserveIT Active Directory Service Account #>
$observeitInstallerPath = "<Enter full path to the root of the new ObserveIT installer here>"
$SQLServer = '<FQDN of the SQL Server>'                                     # FQDN of the SQL Server for ObserveIT
$DNSForestName = "<enter your DNS domain name here>"                        # For example, contoso.local
$Creds = Get-Credential -UserName "<Enter ObserveIT Service Account username here in the DOMAIN\account format>" -Message "Enter ObserveIT Service Account credentials"
$ComputerName = (Get-WmiObject -Class Win32_ComputerSystem).PSComputerName  # This gathers the computer name for further use.
$MachineFQDN = $ComputerName + '.' + $DNSForestName                         # This generates full machine FQDN.
$OutputDestination = 'C:\temp'                                              # Define a destination for the certificate export later on.

<# Validate the credentials are correct.
If you get no error back - the credentials are correct.
If an error comes back - the credentials are incorrect. #>
Start-Process cmd.exe -Credential $creds -NoNewWindow -ArgumentList "/c" -Wait

<# Make sure the ObserveIT installer files are unblocked #>
Get-ChildItem $observeitInstallerPath -Recurse | Foreach-Object {Unblock-File $_.Fullname}

<# First, let's allow Website Categorization module to listen on port 8000 for ObserveIT Application server requests #>
New-NetFirewallRule -DisplayName "ObserveIT Web Categorization module" -Direction Inbound –Protocol TCP –LocalPort 8000 -Action allow

<# Let's define  variables to be used during the ObserveIT Application server installation #>
$ComponentInstallArguments = "/i", ($observeitInstallerPath + '\' + 'WebsiteCat\WebsiteCat_Setup.msi'), '/qb', "/norestart", "DATABASE_SERVER=$SQLServer","DATABASE_LOGON_TYPE=WindowsAccount", "SERVICE_USERNAME=$($Creds.GetNetworkCredential().Domain + '\' + $Creds.GetNetworkCredential().UserName)", "SERVICE_PASSWORD=$($Creds.GetNetworkCredential().Password)", "/leo", "WebSiteCatMSI.log"
Start-Process msiexec.exe -ArgumentList $ComponentInstallArguments -Wait -NoNewWindow

<# Now let's validate the installation completed successfully #>
$TestString = 'WebsiteCat Registration finished Successfully'
Get-Content "$env:USERPROFILE\AppData\Local\Temp\WebsiteCat_CA_Log.txt" | Select-String $TestString

<# Note that by default Website Categorization module is installed without proxy configuration.
If you would like the Website Categorization module to access the Internet through a web proxy,
update the following file: C:\Program Files\ObserveIT\WebsiteCat\Adapters\NetStar\db\etc\gcf1.conf
In particular:
PROXY_HOST=<proxy FQDN or IP address>
PROXY_PORT=<proxy port>
#>

<# Restart ObserveIT Website Categorization module services #>
Get-Service GCF*,Website* | Restart-Service -Force

<# Initiate Website Categorization module database update #>
&'C:\Program Files\ObserveIT\WebsiteCat\WebsiteCat.Manager.exe' -dw

<# Now validate the Website Categorization module component itself is functioning correctly #>
&'C:\Program Files\ObserveIT\WebsiteCat\WebsiteCat.Manager.exe' -URL google.com | Select-Object -Last 1
<# You should expect result similar to this:
[I] Got category "Search Engines & Portals" (oitId=31) for the url "google.com"
#>

<# Next, validate the ObserveIT Application servers can use the Website Categorization module to query for
website categories. Execute the command on an ObserveIT Application server or any other server or machine that
can connect to the ObserveIT Application servers or the ObserveIT load balancer. #>
(Invoke-WebRequest '<http/https>://<ObserveIT FQDN>:<port>/ObserveitApplicationServer/v2/apis/webcat/sites?url=google.com' -UseBasicParsing).Content
<# Expect result similar to this:
{"_status":{"status":200,"code":"it:error:none"},"kind":"it:webcat:site","primary":{"name":"Search Engines & Portals","url":"google.com","groupName":"Search Engines & Portals","groupId":31,"kind":"it:webcat:category","id":0,"createdAt":"2020-05-03T16:02:57","updatedAt":"2020-05-04T13:46:30","createdBy":{"name":"webcat-service"}},"id":0,"site":"google.com","createdAt":"2020-05-03T16:02:57","url":"google.com","updatedAt":"2020-05-04T13:46:30","createdBy":{"name":"webcat-service"}}
#>
```

### Installing the ObserveIT Screenshot Storage Optimizer

The ObserveIT Screenshot Storage Optimizer component cannot be currrently installed automatically. You will need to follow the documentation on [installing the Screenshot Storage Optimizer](https://documentation.observeit.com/installation_guide/installing_the_screenshots_storage_optimizer_.htm).
