# Moving database files to another drive

This document describes steps to move ObserveIT databases on an SQL Server from a single, default drive, to separate drives.

In an example below, 2 new drives are used for ObserveIT databases and transaction log file.

The example below assumes new ObserveIT databases were created on C: drive, under **C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\**.

In this example, E: drive is intended for ObserveIT database files, while the F: drive is intended for the ObserveIT transaction log files.

## Stop access to ObserveIT databases

```SQL
USE MASTER;
GO
ALTER DATABASE ObserveIT_Data
SET SINGLE_USER
WITH ROLLBACK IMMEDIATE;
GO
EXEC MASTER.dbo.sp_detach_db @dbname = N'ObserveIT_Data'
GO
USE MASTER;
GO
ALTER DATABASE ObserveIT
SET SINGLE_USER
WITH ROLLBACK IMMEDIATE;
GO
EXEC MASTER.dbo.sp_detach_db @dbname = N'ObserveIT'
GO
USE MASTER;
GO
ALTER DATABASE ObserveIT_Archive_1
SET SINGLE_USER
WITH ROLLBACK IMMEDIATE;
GO
EXEC MASTER.dbo.sp_detach_db @dbname = N'ObserveIT_Archive_1'
GO
USE MASTER;
GO
ALTER DATABASE ObserveIT_Archive_Template
SET SINGLE_USER
WITH ROLLBACK IMMEDIATE;
GO
EXEC MASTER.dbo.sp_detach_db @dbname = N'ObserveIT_Archive_Template'
GO
USE MASTER;
GO
ALTER DATABASE ObserveIT_Analytics
SET SINGLE_USER
WITH ROLLBACK IMMEDIATE;
GO
EXEC MASTER.dbo.sp_detach_db @dbname = N'ObserveIT_Analytics'
GO
```

In the example below, drive E: is used for database drives, and drive F: is used for transaction log files.

```PowerShell
<# Create folders for ObserveIT databases and transaction logs #>
<# Create a folder on E: drive for ObserveIT databases #>
New-Item -Name MSSQLDATA -Path E: -ItemType Directory

<# Now create a folder on F: drive for the transaction log files #>
New-Item -Name MSSQLLog -Path E: -ItemType Directory

<# Now move the database files to the E: drive #>
Get-ChildItem 'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\' | Where-Object {$_.Name -like "*observeit*" -and $_.Name -like "*mdf"} | Move-Item -Destination E:\MSSQLDATA\

<# Now move the transaction log files to the F: drive #>
Get-ChildItem 'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\' | Where-Object {$_.Name -like "*observeit*" -and $_.Name -like "*ldf"} | Move-Item -Destination F:\MSSQLLog\
```

Last, attach the database files again to the database:

```SQL
CREATE DATABASE [ObserveIT_Data] ON
( FILENAME = N'E:\MSSQLDATA\ObserveIT_Data_Data.mdf' ),
( FILENAME = N'F:\MSSQLLog\ObserveIT_Data_Log.ldf' )
FOR ATTACH
GO
CREATE DATABASE [ObserveIT] ON
( FILENAME = N'E:\MSSQLDATA\ObserveIT_Data.mdf' ),
( FILENAME = N'F:\MSSQLLog\ObserveIT_Log.ldf' )
FOR ATTACH
GO
CREATE DATABASE [ObserveIT_Analytics] ON
( FILENAME = N'E:\MSSQLDATA\ObserveIT_Analytics_Data.mdf' ),
( FILENAME = N'F:\MSSQLLog\ObserveIT_Analytics_Log.ldf' )
FOR ATTACH
GO
CREATE DATABASE [ObserveIT_Archive_1] ON
( FILENAME = N'E:\MSSQLDATA\ObserveIT_Archive_1_Data.mdf' ),
( FILENAME = N'F:\MSSQLLog\ObserveIT_Archive_1_Log.ldf' )
FOR ATTACH
GO
CREATE DATABASE [ObserveIT_Archive_Template] ON
( FILENAME = N'E:\MSSQLDATA\ObserveIT_Archive_Template_Data.mdf' ),
( FILENAME = N'F:\MSSQLLog\ObserveIT_Archive_Template_Log.ldf' )
FOR ATTACH
GO
```
