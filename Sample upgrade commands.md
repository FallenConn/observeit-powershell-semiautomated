# Sample upgrade commands

## Using this document

Here's how you approach this document:

- Download this file to your computer.
- Replace the sample values below with the real-world values in the current environment.
- Copy and paste the commands from this text file into PowerShell-as-administrator window on the machine.

### Gather pre-upgrade information

Prior to the upgrade, ensure the ObserveIT databases are backed up. It also makes sense to create snapshot of all ObserveIT back-end servers.

Next, execute the following PowerShell commands on an ObserveIT Console server and a ObserveIT Application server - once for each. The commands store the information gathered in C:\Temp\oitinfo.log

```PowerShell
<# Command below gets the address of the SQL Server hosting ObserveIT databases #>
Get-Content 'C:\Program Files\observeit\Web\ObserveIT\Web.Config' -ErrorAction SilentlyContinue | Select-String "connect" | Out-File "C:\Temp\oitinfo.log" -Append
<# Commands below get a list of IIS websites on the current server #>
Import-Module WebAdministration
Get-Item IIS:\Sites\ObserveIT* | Select-Object Name, State -ExpandProperty Bindings | Format-Table Name, State, Collection | Out-File "C:\Temp\oitinfo.log" -Append
<# Commands below gather list of Application Pools on the current server #>
Import-Module WebAdministration
Get-Item IIS:\AppPools\ObserveIT* | Out-File "C:\Temp\oitinfo.log" -Append
<# Command below discovers whether the SOAP section is enabled or disabled in the ObserveIT Application server configuration.
This is only necessary on ObserveIT version <v7.10.0 and with TLS termination configured #>
Get-Content 'C:\Program Files\ObserveIT\Web\ObserveITApplicationServer\Web.config' -ErrorAction SilentlyContinue | Select-String "soap" | Out-File "C:\Temp\oitinfo.log" -Append
<# Stop IIS websites to prevent any data writes during the upgrade #>
Get-Website | Stop-Website
```

Next, execute the following queries on the SQL Server hosting ObserveIT databases. This data is useful should the upgrade go wrong.

```SQL
SELECT * FROM ObserveIT.dbo.ApplicationServers -- Gathers list of ObserveIT Application servers
SELECT * FROM ObserveIT.dbo.WebConsoles -- Gathers list of ObserveIT Console servers
SELECT * FROM ObserveIT.dbo.ServicesWebsiteCategories -- Gathers Website Categorization module instances
SELECT * FROM ObserveIT.dbo.FileSystemStorageSettings -- Gathers list of screen capture data stores
```

Lastly, execute the command below on an Website Categorization module server:

```PowerShell
<# Discovers Website Categorization module proxy settings #>
Get-Content 'C:\Program Files\ObserveIT\WebsiteCat\Adapters\NetStar\db\etc\gcf1.conf' | Select-String "proxy" | Out-File "C:\Temp\oitinfo.log" -Append
```

### Extracting ObserveIT installer

If the ObserveIT installer is not yet extracted, and you only have a .zip file downloaded from ObserveIT website, use the commands below to extract it. This assumes the installer is located on C:\Temp on each server. Replace **ObserveITx_Setup_vx.x.x.x.zip** with an actual ObserveIT .zip file name.

```PowerShell
Set-Location C:\Temp
Expand-Archive "C:\Temp\ObserveITx_Setup_vx.x.x.x.zip" -Force
```

### Installing ObserveIT databases

```PowerShell
<# We are installing databases first.
You can install the databases from any server you intend for ObserveIT components, such as
the ObserveIT Console, ObserveIT Application server, or ObserveIT Website Categorization module.
All the roles mentioned above need access to the SQL Server #>

<# Before upgrading the databases, stop all ObserveIT services on all servers, including
ObserveIT Application Servers, ObserveIT Web Management Console, and screen capture data store #>
Get-Service observeit*,screenshot*,website*,gcf*|Stop-Service
iisreset /stop

# First, define the installer path:
$observeitInstallerPath = "<Enter full path to the root of the new ObserveIT installer here>"
<# Specifically, point to the path where all the extracted ObserveIT installer folders are, like 
DB, DB_Analytics, Web, and others. #>

<# Make sure the ObserveIT installer files are unblocked #>
Get-ChildItem $observeitInstallerPath -Recurse | Foreach-Object {Unblock-File $_.Fullname}

<# Now we can call the database installer.
Make sure the server has access to the database server #>
Start-Process "$observeitInstallerPath\DB\SQLPackage.exe" -ArgumentList "/server:<FQDN of the SQL Server>" -Wait
# Look through the output log to validate the installer ran successfully
Get-Content "$observeitInstallerPath\DB\Sql_Setup.txt" | Select-String -Pattern "Package executed successfully"

<# Now run installer for the Analytics database #>
Start-Process "$observeitInstallerPath\DB_Analytics\SQLPackage.exe" -ArgumentList "/server:<FQDN of the SQL Server>" -Wait
<# ...and validate that installation completed successfully as well #>
Get-Content "$observeitInstallerPath\DB_Analytics\Sql_Setup.txt" | Select-String -Pattern "Package executed successfully"
<# This concludes installation of ObserveIT databases. #>
```

### Installing ObserveIT Console

```PowerShell
<# Let's define  variables to be used during the ObserveIT Console installation #>
$observeitInstallerPath = "<Enter full path to the root of the new ObserveIT installer here>"
$SQLServer = '<FQDN of the SQL Server>'                                     # FQDN of the SQL Server for ObserveIT.
$DNSForestName = "<enter your DNS domain name here>"                        # For example, contoso.local
$Creds = Get-Credential -UserName "<Enter ObserveIT Service Account username here in the DOMAIN\account format>" -Message "Enter ObserveIT Service Account credentials"
$WebSiteName = 'ObserveITWebConsole'                                        # Name of the website for the ObserveIT Console. ObserveITWebConsole is the preferred default.
$WebSitePort = "443"                                                        # Port for the ObserveIT Console website. 443 is the preferred default.
$WebSiteProtocol = "https"                                                  # Protocol for the ObserveIT Console website. https is the preferred default.
$ComputerName = (Get-WmiObject -Class Win32_ComputerSystem).PSComputerName  # This gathers the computer name for further use.
$MachineFQDN = $ComputerName + '.' + $DNSForestName                         # This generates full machine FQDN.
$ApplicationPool = "IIS:\AppPools\$WebSiteName"                             # Generates the IIS Application Pool name.
$WebSiteBinding = ":" + $WebSitePort + ":"                                  # Generates the IIS binding string.
$OutputDestination = 'C:\temp'                                              # Define a destination for the certificate export later on.

<# Validate the credentials are correct.
If you get no error back - the credentials are correct.
If an error comes back - the credentials are incorrect. #>
Start-Process cmd.exe -Credential $creds -NoNewWindow -ArgumentList "/c" -Wait

<# Here, we prepare for the ObserveIT Console. #>
<# Install Microsoft IIS and required components #>
Install-WindowsFeature Web-Server, Web-WebServer, Web-Common-Http, Web-Default-Doc, Web-Dir-Browsing, Web-Http-Errors, Web-Static-Content, Web-Stat-Compression, Web-Security, Web-Filtering, Web-App-Dev, Web-Net-Ext45, Web-Asp, Web-Asp-Net45, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Mgmt-Tools, Web-Mgmt-Compat, Web-Mgmt-Console, NET-WCF-Services45, NET-WCF-HTTP-Activation45 –IncludeManagementTools

<# Make sure the ObserveIT installer files are unblocked #>
Get-ChildItem $observeitInstallerPath -Recurse | Foreach-Object {Unblock-File $_.Fullname}

<# With the IIS components installed, and configured, we can install ObserveIT prerequisites packages.
First, we are installing the NodeJS and .Net Core. #>
$NodeJSInstaller = $observeitInstallerPath + '\Web\PreRequisite_nodeServices.exe'
$ComponentInstallArguments = "wconly=1", "sqlcli=1", "/install", "/quiet", "/norestart" ,"/log PreRequisite_nodeServices.log"
Start-Process $NodeJSInstaller -ArgumentList $ComponentInstallArguments -Wait

<# Start ObserveIT services now #>
Get-Service observeit*,screenshot*,website*,gcf*|Start-Service
Get-Service WAS | Start-Service

<# Let's confirm DefaultAppPool exists. If not - let's create it. #>
Import-Module WebAdministration
if (!(Get-Item IIS:\AppPools\DefaultAppPool)) { New-Item IIS:\AppPools\DefaultAppPool; Write-Output "Adding DefaultAppPool" } else { Write-Output "DefaultAppPool exists. Continuing." }

<# Now we can install the ObserveIT Console itself. This will install the ObserveIT Console
and the Advanced ObserveIT Console components. #>
$ComponentInstallArguments = "/i", ($observeitInstallerPath + '\Web\WebConsole\ObserveIT.WebConsoleSetup.msi'), "/qb", "/norestart", "DATABASE_SERVER=$SQLServer", "TARGETAPPPOOL=$WebSiteName", "TARGETSITE=$WebSiteName", "EXTRACTMICROSERVICES=True", "DATABASE_LOGON_TYPE=WindowsAccount", "SERVICE_USERNAME=$($Creds.GetNetworkCredential().Domain + '\' + $Creds.GetNetworkCredential().UserName)", "SERVICE_PASSWORD=$($Creds.GetNetworkCredential().Password)","/leo", ".\WebConsoleMSI.log"
Start-Process msiexec.exe -ArgumentList $ComponentInstallArguments -Wait -NoNewWindow

<# Validate ObserveIT Web Management Console was installed correctly. #>
if ((Get-Content $env:LOCALAPPDATA\temp\WebConsole_CA_Log.txt | Select-String Done -CaseSensitive | Select-Object -Last 10 | Measure-Object).Count -eq 10) { Write-Output "ObserveIT Web Management Console installation succeeded." } else { Write-Output "ObserveIT Web Management Console installation might not have succeeded, please examine trace logs."}

<# DefaultAppPool is no longer needed. Let's remove it. #>
Import-Module WebAdministration
if ((Get-Item IIS:\AppPools\DefaultAppPool)) { Remove-Item IIS:\AppPools\DefaultAppPool; Write-Output "Removing DefaultAppPool" } else { Write-Output "DefaultAppPool is already gone. Continuing." }

<# Extract the Advanced ObserveIT Web Management Console #>
Set-Location "C:\Program Files\ObserveIT\Web\V2\apis"
Get-ChildItem *.zip | foreach {Expand-Archive -LiteralPath $_.FullName -DestinationPath $($_.Directory.ToString() + '\' + $_.BaseName.ToString()) -Force}
Set-Location "C:\Program Files\ObserveIT\Web\V2\apps"
Get-ChildItem *.zip | foreach {Expand-Archive -LiteralPath $_.FullName -DestinationPath $($_.Directory.ToString() + '\' + $_.BaseName.ToString()) -Force}

iisreset /start

<# Start IIS website to allow for communications #>
Get-Website $WebSiteName | Start-Website

<# Now, let's validate the installation succeeded.
For that, we will make sure PowerShell ignores TLS trust (so we can execute the command
against localhost) and will then make sure the ObserveIT Console login page loads. #>
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
Import-Module WebAdministration
Get-IISSite $WebSiteName | Start-IISSite
$CurrentURL = $WebSiteProtocol + '://localhost:' + $WebSitePort + '/ObserveIT/FormLoginAuth.aspx?UserDefaultPage=True'
(Invoke-WebRequest $CurrentURL).RawContent -match 'ObserveIT - Login Page'
<# Expect to receive True as a success response. #>

<# This concludes installation of the ObserveIT Console. #>
```

### Installing ObserveIT Application server

```PowerShell
<# The procedure of installing ObserveIT Application server is very similar to the ObserveIT Console.
Nearly the same, in fact, except the website and Application Pools names #>

<# Let's define  variables to be used during the ObserveIT Application server installation #>
$observeitInstallerPath = "<Enter full path to the root of the new ObserveIT installer here>"
$SQLServer = '<FQDN of the SQL Server>'                                     # FQDN of the SQL Server for ObserveIT
$DNSForestName = "<enter your DNS domain name here>"                        # For example, contoso.local
$Creds = Get-Credential -UserName "<Enter ObserveIT Service Account username here in the DOMAIN\account format>" -Message "Enter ObserveIT Service Account credentials"
$WebSiteName = 'ObserveITApplicationServer'                                 # Name of the website for the ObserveIT Console. ObserveITWebConsole is the preferred default.
$WebSitePort = "443"                                                        # Port for the ObserveIT Console website. 443 is the preferred default.
$WebSiteProtocol = "https"                                                  # Protocol for the ObserveIT Console website. https is the preferred default.
$ComputerName = (Get-WmiObject -Class Win32_ComputerSystem).PSComputerName  # This gathers the computer name for further use.
$MachineFQDN = $ComputerName + '.' + $DNSForestName                         # This generates full machine FQDN.
$ApplicationPool = "IIS:\AppPools\$WebSiteName"                             # Generates the IIS Application Pool name.
$WebSiteBinding = ":" + $WebSitePort + ":"                                  # Generates the IIS binding string.
$OutputDestination = 'C:\temp'                                              # Define a destination for the certificate export later on.

<# Validate the credentials are correct.
If you get no error back - the credentials are correct.
If an error comes back - the credentials are incorrect. #>
Start-Process cmd.exe -Credential $creds -NoNewWindow -ArgumentList "/c" -Wait

<# Here, we prepare for the ObserveIT Console. #>
<# Install Microsoft IIS and required components #>
Install-WindowsFeature Web-Server, Web-WebServer, Web-Common-Http, Web-Default-Doc, Web-Dir-Browsing, Web-Http-Errors, Web-Static-Content, Web-Stat-Compression, Web-Security, Web-Filtering, Web-App-Dev, Web-Net-Ext45, Web-Asp, Web-Asp-Net45, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Mgmt-Tools, Web-Mgmt-Compat, Web-Mgmt-Console, NET-WCF-Services45, NET-WCF-HTTP-Activation45 –IncludeManagementTools

<# Make sure the ObserveIT installer files are unblocked #>
Get-ChildItem $observeitInstallerPath -Recurse | Foreach-Object {Unblock-File $_.Fullname}

<# With the IIS components installed, and configured, we can install ObserveIT prerequisites packages.
Let's install the NodeJS and .Net Core. #>
$NodeJSInstaller = $observeitInstallerPath + '\Web\PreRequisite_nodeServices.exe'
$ComponentInstallArguments = "common=1", "/install", "/quiet", "/norestart", "/log PreRequisite_nodeService.log"
Start-Process $NodeJSInstaller -ArgumentList $ComponentInstallArguments -Wait

<# Start ObserveIT services now #>
Get-Service observeit*,screenshot*,website*,gcf*|Start-Service
Get-Service WAS | Start-Service
iisreset /start

<# Let's confirm DefaultAppPool exists. If not - let's create it. #>
Import-Module WebAdministration
if (!(Get-Item IIS:\AppPools\DefaultAppPool)) { New-Item IIS:\AppPools\DefaultAppPool; Write-Output "Adding DefaultAppPool" } else { Write-Output "DefaultAppPool exists. Continuing." }

<# Now we can install the ObserveIT Application server itself.#>
$ComponentInstallArguments = "/i", ($observeitInstallerPath + '\Web\AppServer\ObserveIT.AppServerSetup.msi'), "/qb", "/norestart", "DATABASE_SERVER=$SQLServer", "TARGETAPPPOOL=$WebSiteName", "TARGETSITE=$WebSiteName", "DATABASE_LOGON_TYPE=WindowsAccount", "SERVICE_USERNAME=$($Creds.GetNetworkCredential().Domain + '\' + $Creds.GetNetworkCredential().UserName)", "SERVICE_PASSWORD=$($Creds.GetNetworkCredential().Password)","/leo", ".\AppServerMSI.log"
Start-Process msiexec.exe -ArgumentList $ComponentInstallArguments -Wait -NoNewWindow

<# Validate ObserveIT Application Server was installed correctly. #>
if ((Get-Content $env:LOCALAPPDATA\temp\AppServer_CA_Log.txt | Select-String Done -CaseSensitive | Select-Object -Last 3 | Measure-Object).Count -eq 3) { Write-Output "ObserveIT Application Server installation succeeded." } else { Write-Output "ObserveIT Application Server installation might not have succeeded, please examine trace logs." }

<# DefaultAppPool is no longer needed. Let's remove it. #>
Import-Module WebAdministration
if ((Get-Item IIS:\AppPools\DefaultAppPool)) { Remove-Item IIS:\AppPools\DefaultAppPool; Write-Output "Removing DefaultAppPool" } else { Write-Output "DefaultAppPool is already gone. Continuing." }

<# Start IIS website to allow for communications #>
Get-Website $WebSiteName | Start-Website

<# Now, let's validate the installation succeeded.
For that, we will make sure PowerShell ignores TLS trust (so we can execute the command
against localhost) and will then make sure ObserveIT Application server health check is successful. #>
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
Import-Module WebAdministration
Get-IISSite $WebSiteName | Start-IISSite
$CurrentURL = $WebSiteProtocol + '://localhost:' + $WebSitePort + '/ObserveitApplicationServer/v2/apis/health/_health'
Invoke-WebRequest $CurrentURL -UseBasicParsing
<# You should get code 200 as a response. #>
```

#### Update ObserveIT management console URL

Use SQL Management Studio to execute these queries:

```SQL
UPDATE ObserveIT.dbo.WebConsoles SET WebConsoleFqdnUrl = '<# http/https://<# FQDN of the ObserveIT management console server #>:<# port #>/ObserveIT #>';
```

#### Update Application server URLs in the database

```SQL
-- Update the load balancer URL
UPDATE ObserveIT.dbo.DatabaseConfiguration
SET DatabaseConfigurationValue = '<# http/https://<FQDN>/<# port #>/ObserveITApplicationServer #>'
WHERE DatabaseConfigurationKey = 'AppServerUrl'
GO

-- Update addresses of each specific Application server
UPDATE ObserveIT.dbo.ApplicationServers
SET ApplicationServerUrl = '<# http/https://<FQDN of load balancer>/<# port #>/ObserveITApplicationServer #>'
WHERE ServerName = '<# hostname of this Application server #>'
GO

UPDATE ObserveIT.dbo.ApplicationServers
SET ApplicationServerInternalUrl = '<# http/https://<FQDN of this Application server>/<# port #>/ObserveITApplicationServer #>'
WHERE ServerName = '<# hostname of this Application server #>'
GO
```

Restart IIS now. This concludes installation of the ObserveIT Application server.

### Installing ObserveIT Website Categorization module

```PowerShell
<# Let's define  variables to be used during the ObserveIT Application server installation #>
$observeitInstallerPath = "<Enter full path to the root of the new ObserveIT installer here>"
$SQLServer = '<FQDN of the SQL Server>'                                     # FQDN of the SQL Server for ObserveIT
$DNSForestName = "<enter your DNS domain name here>"                        # For example, contoso.local
$Creds = Get-Credential -UserName "<Enter ObserveIT Service Account username here in the DOMAIN\account format>" -Message "Enter ObserveIT Service Account credentials"
$ComputerName = (Get-WmiObject -Class Win32_ComputerSystem).PSComputerName  # This gathers the computer name for further use.
$MachineFQDN = $ComputerName + '.' + $DNSForestName                         # This generates full machine FQDN.
$OutputDestination = 'C:\temp'                                              # Define a destination for the certificate export later on.

<# Validate the credentials are correct.
If you get no error back - the credentials are correct.
If an error comes back - the credentials are incorrect. #>
Start-Process cmd.exe -Credential $creds -NoNewWindow -ArgumentList "/c" -Wait

<# Make sure the ObserveIT installer files are unblocked #>
Get-ChildItem $observeitInstallerPath -Recurse | Foreach-Object {Unblock-File $_.Fullname}

<# Let's define  variables to be used during the ObserveIT Application server installation #>
$ComponentInstallArguments = "/i", ($observeitInstallerPath + '\' + 'WebsiteCat\WebsiteCat_Setup.msi'), '/qb', "/norestart", "DATABASE_SERVER=$SQLServer","DATABASE_LOGON_TYPE=WindowsAccount", "SERVICE_USERNAME=$($Creds.GetNetworkCredential().Domain + '\' + $Creds.GetNetworkCredential().UserName)", "SERVICE_PASSWORD=$($Creds.GetNetworkCredential().Password)", "/leo", "WebSiteCatMSI.log"
Start-Process msiexec.exe -ArgumentList $ComponentInstallArguments -Wait -NoNewWindow

<# Now let's validate the installation completed successfully #>
$TestString = 'WebsiteCat Registration finished Successfully'
Get-Content "$env:USERPROFILE\AppData\Local\Temp\WebsiteCat_CA_Log.txt" | Select-String $TestString

<# Note that by default Website Categorization module is installed without proxy configuration.
If you would like the Website Categorization module to access the Internet through a web proxy,
update the following file: C:\Program Files\ObserveIT\WebsiteCat\Adapters\NetStar\db\etc\gcf1.conf
In particular:
PROXY_HOST=<proxy FQDN or IP address>
PROXY_PORT=<proxy port>
#>

<# Restart ObserveIT Website Categorization module services #>
Get-Service GCF*,Website* | Restart-Service -Force

<# Initiate Website Categorization module database update #>
&'C:\Program Files\ObserveIT\WebsiteCat\WebsiteCat.Manager.exe' -dw

<# Now validate the Website Categorization module component itself is functioning correctly #>
&'C:\Program Files\ObserveIT\WebsiteCat\WebsiteCat.Manager.exe' -URL google.com | Select-Object -Last 1
<# You should expect result similar to this:
[I] Got category "Search Engines & Portals" (oitId=31) for the url "google.com"
#>

<# Next, validate the ObserveIT Application servers can use the Website Categorization module to query for
website categories. Execute the command on an ObserveIT Application server or any other server or machine that
can connect to the ObserveIT Application servers or the ObserveIT load balancer. #>
(Invoke-WebRequest '<http/https>://<ObserveIT FQDN>:<port>/ObserveitApplicationServer/v2/apis/webcat/sites?url=google.com' -UseBasicParsing).Content
<# Expect result similar to this:
{"_status":{"status":200,"code":"it:error:none"},"kind":"it:webcat:site","primary":{"name":"Search Engines & Portals","url":"google.com","groupName":"Search Engines & Portals","groupId":31,"kind":"it:webcat:category","id":0,"createdAt":"2020-05-03T16:02:57","updatedAt":"2020-05-04T13:46:30","createdBy":{"name":"webcat-service"}},"id":0,"site":"google.com","createdAt":"2020-05-03T16:02:57","url":"google.com","updatedAt":"2020-05-04T13:46:30","createdBy":{"name":"webcat-service"}}
#>
```

### Installing the ObserveIT Screenshot Storage Optimizer

The ObserveIT Screenshot Storage Optimizer component cannot be currrently installed automatically. You will need to follow the documentation on [installing the Screenshot Storage Optimizer](https://documentation.observeit.com/installation_guide/installing_the_screenshots_storage_optimizer_.htm).
