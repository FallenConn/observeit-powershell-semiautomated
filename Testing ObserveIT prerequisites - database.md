# Testing ObserveIT prerequisites - Database

Follow the steps below on the SQL Server that will host the ObserveIT databases. Unless otherwise specified, all the commands in this section are SQL queries to be executed in the SQL Management Studio.

Validate that:

## The SQL Server is either SQL Server Standard Edition or SQL Server Enterprise Edition

```SQL
Select @@version
```

Expect result similar to below:

```Text
Microsoft SQL Server 2017 (RTM-CU13-OD) (KB4483666) - 14.0.3049.1 (X64) 
    Dec 15 2018 11:16:42 
    Copyright (C) 2017 Microsoft Corporation
    Standard Edition (64-bit) on Windows Server 2016 Datacenter 10.0 <X64> (Build 14393: ) (Hypervisor)
```

## The SQL Server is of a supported version

Visit the [Supported Platforms](https://documentation.observeit.com/installation_guide/supported_platforms.htm).

## The SQL Server is installed with a required collation

Collations supported by ObserveIT are **SQL_Latin1_General_CP1_CI_AS** or **Latin1_General_CI_AS**.

```SQL
SELECT CONVERT (varchar, SERVERPROPERTY('collation')) AS 'Server Collation';
```

## If upgrading, at least 5 ObserveIT databases are present

The command below retrieves list of databases on the current SQL Server with name starting with *ObserveIT*

```SQL
SELECT * FROM master.sys.sysdatabases WHERE name LIKE 'ObserveIT%'
```

Exepect result similar to this:

| name                       |
| -------------------------- |
| ObserveIT_Data             |
| ObserveIT_Archive_Template |
| ObserveIT_Archive_1        |
| ObserveIT_Analytics        |
| ObserveIT                  |

## If upgrading, validate the supported collation for ObserveIT databases

```SQL
USE Master
GO
SELECT
 NAME, 
 COLLATION_NAME
FROM sys.Databases
 ORDER BY DATABASE_ID ASC
GO
```

Expect result similar to this:

| NAME                       | COLLATION_NAME               |
| -------------------------- | ---------------------------- |
| master                     | SQL_Latin1_General_CP1_CI_AS |
| tempdb                     | SQL_Latin1_General_CP1_CI_AS |
| model                      | SQL_Latin1_General_CP1_CI_AS |
| msdb                       | SQL_Latin1_General_CP1_CI_AS |
| rdsadmin                   | SQL_Latin1_General_CP1_CI_AS |
| ObserveIT_Data             | SQL_Latin1_General_CP1_CI_AS |
| ObserveIT                  | SQL_Latin1_General_CP1_CI_AS |
| ObserveIT_Archive_1        | SQL_Latin1_General_CP1_CI_AS |
| ObserveIT_Archive_Template | SQL_Latin1_General_CP1_CI_AS |
| ObserveIT_Analytics        | SQL_Latin1_General_CP1_CI_AS |

## Find default location for ObserveIT database and log files

```SQL
SELECT
  SERVERPROPERTY('InstanceDefaultDataPath') AS 'Data Files',
  SERVERPROPERTY('InstanceDefaultLogPath') AS 'Log Files'
```

Expect result similar to this:

| Data Files     | Log Files       |
| -------------- | --------------- |
| D:\SQLDB\DATA\ | E:\SQLLOG\DATA\ |

Make sure the default location is **not** the C drive. Installing ObserveIT databases to the system drive could cause the system to fail and would degrade the Operating System performance.

## If upgrading, validate ObserveIT databases are not stored on the system drive

```SQL
SELECT
    db.name AS DBName,
    type_desc AS FileType,
    Physical_Name AS Location
FROM
    sys.master_files mf
INNER JOIN 
    sys.databases db ON db.database_id = mf.database_id
WHERE db.name LIKE 'ObserveIT%'
```

Expect a result similar to this:

| DBName                     | FileType | Location                                              |
| -------------------------- | -------- | ----------------------------------------------------- |
| ObserveIT                  | ROWS     | d:\rdsdbdata\data\ObserveIT_Data.mdf                  |
| ObserveIT                  | LOG      | d:\rdsdbdata\data\ObserveIT_Log.ldf                   |
| ObserveIT_Analytics        | ROWS     | d:\rdsdbdata\data\ObserveIT_Analytics_Data.mdf        |
| ObserveIT_Analytics        | LOG      | d:\rdsdbdata\data\ObserveIT_Analytics_Log.ldf         |
| ObserveIT_Archive_1        | ROWS     | d:\rdsdbdata\data\ObserveIT_Archive_1_Data.mdf        |
| ObserveIT_Archive_1        | LOG      | d:\rdsdbdata\data\ObserveIT_Archive_1_Log.ldf         |
| ObserveIT_Archive_Template | ROWS     | d:\rdsdbdata\data\ObserveIT_Archive_Template_Data.mdf |
| ObserveIT_Archive_Template | LOG      | d:\rdsdbdata\data\ObserveIT_Archive_Template_Log.ldf  |
| ObserveIT_Data             | ROWS     | d:\rdsdbdata\data\ObserveIT_Data_Data.mdf             |
| ObserveIT_Data             | LOG      | d:\rdsdbdata\data\ObserveIT_Data_Log.ldf              |

Validate no file is located on C or other system drives. Allow ObserveIT databases to be stored on system volumes is likely to result in a system down state.
