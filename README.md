# ObserveIT manual installation PowerShell commands

This page describes your basic commands require to uninstall or install ObserveIT. Use it to make your life a bit easier during the installation/upgrade cycle.

>**NOTE**
>
>This page was updated to support the changes in
>the prerequisites installer for ObserveIT v7.11.0.

# Table of Contents

- [Notes](#markdown-header-notes)
- [Upgrading and uninstalling ObserveIT](#markdown-header-upgrading-and-uninstalling-observeit)
    - [Gather current information about the environment](#markdown-header-gather-current-information-about-the-environment)
    - [Uninstall ObserveIT components](#markdown-header-uninstall-observeit-components)
- [Upgrading and installing ObserveIT](#markdown-header-upgrading-and-installing-observeit)
    - [Installing ObserveIT databases](#markdown-header-installing-observeit-databases)
    - [Installing ObserveIT components](#markdown-header-installing-observeit-components)
        - [Installing ObserveIT Application Server](#markdown-header-installing-observeit-application-server)
        - [Installing ObserveIT Console](#markdown-header-installing-observeit-console)
        - [Installing the ObserveIT Website Categorization module](#markdown-header-installing-the-observeit-website-categorization-module)
        - [Installing the Install Screenshot Storage Optimizer](#markdown-header-installing-the-install-screenshot-storage-optimizer)
- [Additional helpers](#markdown-header-additional-helpers)

## Notes

## Document assumptions

This document makes the following assumptions:

- You are deploying or upgrading ObserveIT on Windows Server 2016 or higher.
- You are using https for ObserveIT Agent - ObserveIT Application Server communications.
- You are installing ObserveIT Application Server on a separate machine with no other ObserveIT components.
- You are using https for ObserveIT Console communications.
- You are installing ObserveIT Console on a separate machine with no other ObserveIT components.
- You are installing ObserveIT to work using an Active Directory Service Account.

## Other notes

- This page describes commands necessary to install ObserveIT. Often these commands will be used during the upgrade procedure.
- The values in this document are generalized. For example, the ObserveIT installer file name is **ObserveITx_Setup_vx.x.x.x.zip**, and instead of the Active Directory Service Account name you will find the generalized **DOMAIN\account** value. Before beginning work, replace the generalized values with the actual values, such as the installer name and the Service Account name.
- This document assumes the ObserveIT installer .zip file is located under **C:\Temp**.
- Except for a few SQL queries, all the commands in this guide are PowerShell commands. The expectation is for you to run PowerShell as administrator and execute the commands within that window.
- This document assumes you are executing the commands on Windows Server 2016 or higher. Absolute most commands will work on Windows Server 2012/R2 as well, but may require minor adjustements.
- The **Expand-Archive** command is not available on Windows Server 2012/R2 or lower.

## How to use this page

The best way is to copy the code below to a text editor and replace the path of your ObserveIT installation folder (usually CTRL+H on your keyboard in most text editors). You may need to replace some other values as well, such as accounts.

Here's the usual workflow:

- Download ObserveIT installer >
- Place the installation **.zip** file in **C:\Temp** >
- Replace the values for the variables at the very start of the path >
- Run PowerShell as administrator >
- Execute the commands necessary for your deployment.

## Definitions

This section defines variables for the commands to operate. Do note that the PowerShell variables themselves don't really matter, and you are not expected to paste them into the PowerShell window. They are intended for you to see what are the custom values defined on this page. Use Find and Replace (CTRL+H) feature of your text editor to change all the values (such as ObserveIT installer path) across the entire document.

- Root path where the ObserveIT installer .zip file is placed: **C:\Temp**
- Path where ObserveIT installer was extracted to: **C:\Temp\ObserveITx_Setup_vx.x.x.x**
- Protocol for the ObserveIT websites: **https**
- Port for ObserveIT Application Server website: **yyy**
- Port for ObserveIT Console website: **zzz**
- ObserveIT service account name: **DOMAIN\account**
- Application Server Product code: **{aaaa-aaaa-aaaa-aaaaaaaaaaaa}**. This is used to uninstall the Application Server from the command line.
- Web Console Product code: **{bbbb-bbbb-bbbb-bbbbbbbbbbbb}**. This is used to uninstall the Web Console from the command line.
- Web Categorization Module Product code: **{cccc-cccc-cccc-cccccccccccc}**. This is used to uninstall the Web Categorization Module from the command line.
- Screenshot Storage Optimizer Product code: **{dddd-dddd-dddd-dddddddddddd}**. This is used to uninstall the Screenshot Storage Optimizer from the command line.

## Information to collect before an upgrade

When upgrading an ObserveIT deployment, collect the following information prior to the altering the environment:

- Does the organization's administrator has access to **sa**-level credentials for the SQL Server hosting ObserveIT databases?
- FQDN and IP address for the ObserveIT Web Console machine(s).
- FQDN and IP address for the ObserveIT Application Server machines.
- FQDN and IP address for the SQL Server machine hosting ObserveIT databases. If using multiple SQL instances - 
- If required, IP address/FQDN, and a port for the Internet-access proxy.
- Name of the Service Account running ObserveIT services.

## Upgrading and uninstalling ObserveIT

This page describes commands necessary to uninstall ObserveIT. Most often these commands will be used during the upgrade procedure.

**NOTE**: This document assumes you are executing the commands on Windows Server 2016 or higher. Absolute most commands will work on Windows Server 2012/R2 as well, but may require minor adjustements.
**NOTE**: The **Expand-Archive** command is not available on Windows Server 2012/R2 or lower.

Before an upgrade, please gather the following information. In particular:

- FQDN and IP address for the ObserveIT Web Console machine(s).
- FQDN and IP address for the ObserveIT Application Server machines.
- FQDN and IP address for the SQL Server machine hosting ObserveIT databases. If using multiple SQL instances - include instance name.
- Name of the Service Account running ObserveIT services.
- Whether TLS termination used in the environment.
- If required, IP address/FQDN, and a port for the Internet-access proxy (command below).

When upgrading ObserveIT, after finishing with this section of the document, proceed to the Installing ObserveIT section.

## Gather current information about the environment

### Getting the address of the SQL Server

Execute the following command using PowerShell on the ObserveIT Console machine to discover the FQDN of the SQL Server ObserveIT connects to. This information is pulled from the ObserveIT Console's configuration file.

```PowerShell
Get-Content 'C:\Program Files\observeit\Web\ObserveIT\Web.Config' | Select-String "connect" | Out-File "C:\Temp\oitinfo.log" -Append
```

### Getting ObserveIT website name and assigned port

Execute the below command on all ObserveIT web components (i.e. ObserveIT Console and Application Servers) to discover the ports currently assigned to each website.

```PowerShell
Import-Module WebAdministration
Get-Item IIS:\Sites\ObserveIT* | Select-Object Name, State -ExpandProperty Bindings | Format-Table Name, State, Collection | Out-File "C:\Temp\oitinfo.log" -Append
```

### Getting ObserveIT Application Pools name and status

Execute the below command on all ObserveIT web components (i.e. ObserveIT Console and Application Servers) to discover the name and state of the ObserveIT Application Pools.

```PowerShell
Import-Module WebAdministration
Get-Item IIS:\AppPools\ObserveIT* | Out-File "C:\Temp\oitinfo.log" -Append
```

### Gathering ObserveIT information from the database

Execute the following commands using SQL Management Studio connected to the SQL Server hosting ObserveIT databases.

#### Get a list of currently-installed ObserveIT Application Servers

```SQL
SELECT * FROM ObserveIT.dbo.ApplicationServers
```

#### Get a list of currently-installed ObserveIT Consoles

```SQL
SELECT * FROM ObserveIT.dbo.WebConsoles
```

#### Get a list of currently-installed Website Categorization modules

```SQL
SELECT * FROM ObserveIT.dbo.ServicesWebsiteCategories
```

#### Get a list of current file storages

```SQL
SELECT * FROM ObserveIT.dbo.FileSystemStorageSettings
```

### Gathering the status of the SOAP extensions on the Application Servers

Execute the following command in a PowerShell window on the ObserveIT Application Servers to verify whether the SOAP extensions are commented out or removed in the IIS configuration file.
This is important if load balancer TLS termination is configured:

- If TLS termination is configured, the SOAP extensions section should be commented out, starting with the **<!--** symbols. In the **dbo.ApplicationServers** table, the **ApplicationServerEnableWSE** setting should be set to **False**.
- If TLS termination is not configured, the SOAP extensions section should *not* be commented out, and **ApplicationServerEnableWSE** should be set to **True**.

```PowerShell
Get-Content 'C:\Program Files\ObserveIT\Web\ObserveITApplicationServer\Web.config' | Select-String "soap" | Out-File "C:\Temp\oitinfo.log" -Append
```

If the SOAP extensions section is not commented out - meaning TLS termination is not configured - the result will look like this:

```Text
<soapExtensionTypes>
</soapExtensionTypes>
```

If the SOAP extensions section is commented out - meaning TLS termination is configured - the result will look like this:

```Text
<!-- soapExtensionTypes>
</soapExtensionTypes -->
```

### Gathering the details of the Website Categorization module proxy

Execute the following command in a PowerShell window on the Website Categorization module machine to discover Internet proxy configuration:

```PowerShell
Get-Content 'C:\Program Files\ObserveIT\WebsiteCat\Adapters\NetStar\db\etc\gcf1.conf' | Select-String "proxy" | Out-File "C:\Temp\oitinfo.log" -Append
```

### Gathering Product Codes of products on ObserveIT servers

Execute the following command ObserveIT machines to discover the Product Code for ObserveIT components on the current machine.
Use the returned Product Code in following commands to uninstall ObserveIT components directly through PowerShell.
The commands below rely on ObserveIT installer .zip file located at **C:\Temp**.
**NOTE**: In the template below, ObserveIT installer name is denoted as *ObserveITx_Setup_vx.x.x.*. Replace all values with the name of the ObserveIT installer (such as *ObserveIT_Setup_v7.9.1.73*)

#### Extract ObserveIT installer

```PowerShell
Set-Location C:\Temp
Expand-Archive "C:\Temp\ObserveITx_Setup_vx.x.x.x.zip" -Force
```

#### Discover ObserveIT products installed on the current machine

```PowerShell
Set-Location "C:\Temp\ObserveITx_Setup_vx.x.x.x\Utilities"
Expand-Archive .\msiinv.zip -Force
Set-Location .\msiinv\
.\msiinv.exe -p ObserveIT | Select-String 'Product code' -Context 1 | Out-File "C:\Temp\oitinfo.log" -Append
.\msiinv.exe -p Websitecat | Select-String 'Product code' -Context 1 | Out-File "C:\Temp\oitinfo.log" -Append
```

## Uninstall ObserveIT components

### Uninstall ObserveIT Application Server

Replace the **x-xxxx-xxxx-xxxxxxxxxxx** value below with the Product Code of the Application Server from the step above.
On each ObserveIT Application Server machine, start PowerShell as administrator.
Paste the command below into the PowerShell window to begin uninstallation.
After starting the uninstallation, the command below will switch to outputting the uninstallation log. With the uninstall finished, press CTRL+C on the keyboard to get back to prompt.

```PowerShell
iisreset /stop
Get-Service WAS | Start-Service
Start-Sleep -Seconds 3
Start-Process msiexec.exe -ArgumentList '/qb /x "{aaaa-aaaa-aaaa-aaaaaaaaaaaa}"'
Get-Content $env:TEMP\"AppServer_CA_Log.txt" -Wait -Last 10
iisreset /start
```

### Uninstall ObserveIT Console

Replace the **xx-xxxx-xxxx-xxxxxxxxxxx** value below with the Product Code of the ObserveIT Console from the step above.
On each ObserveIT Console machine, start PowerShell as administrator.
Paste the command below into the PowerShell window to begin uninstallation.
After starting the uninstallation, the command below will switch to outputting the uninstallation log. With the uninstall finished, press CTRL+C on the keyboard to get back to prompt.

```PowerShell
iisreset /stop
Get-Service WAS | Start-Service
Start-Sleep -Seconds 3
Start-Process msiexec.exe -ArgumentList '/qb /x "{bbbb-bbbb-bbbb-bbbbbbbbbbbb}"'
Get-Content $env:TEMP\"WebConsole_CA_Log.txt" -Wait -Last 10
iisreset /start
```

### Uninstall ObserveIT Screenshot Storage Optimizer

Replace the **xxx-xxxx-xxxx-xxxxxxxxxxx** value below with the Product Code of the Screenshot Storage Optimizer from the step above.
On each Screenshot Storage Optimizer machine, start PowerShell as administrator.
Paste the command below into the PowerShell window to begin uninstallation.
After starting the uninstallation, the command below will switch to outputting the uninstallation log. With the uninstall finished, press CTRL+C on the keyboard to get back to prompt.

```PowerShell
Start-Process msiexec.exe -ArgumentList '/qb /x "{cccc-cccc-cccc-cccccccccccc}"'
Get-Content $env:TEMP\"ScreenshotsStorageOptimizer_CA_Log.txt" -Wait -Last 10
```

### Uninstall ObserveIT Website Categorization module

Replace the **xxxx-xxxx-xxxx-xxxxxxxxxxx** value below with the Product Code of the Website Categorization module from the step above.
On each Website Categorization module machine, start PowerShell as administrator.
Paste the command below into the PowerShell window to begin uninstallation.
After starting the uninstallation, the command below will switch to outputting the uninstallation log. With the uninstall finished, press CTRL+C on the keyboard to get back to prompt.

```PowerShell
Start-Process msiexec.exe -ArgumentList '/qb /x "{dddd-dddd-dddd-dddddddddddd}"'
Get-Content $env:TEMP\"WebsiteCat_CA_Log.txt" -Wait -Last 10
```

## Upgrading and installing ObserveIT

## Running PowerShell as ObserveIT Service Account

The command below will start an elevated PowerShell prompt (i.e. as administrator) as the Active Directory Service Account running ObserveIT services. This is required to allow ObserveIT component installer to have enough permissions on the current machine during the installation.

**NOTE**: This step is not required when not using an Active Directory Service Account to run ObserveIT services.

```PowerShell
Start-Process powershell.exe -Credential "DOMAIN\account" -NoNewWindow -ArgumentList "Start-Process powershell.exe -Verb runAs"
```

## Installing ObserveIT databases

To install/upgrade ObserveIT databases, run the following commands on the ObserveIT Console machine. The steps to install/upgrade are:

- If using an Active Directory Service Account for ObserveIT services, start PowerShell as the ObserveIT Service Account. Alternatively, log into the RDP session using the ObserveIT Service Account.
- Run the installer for the main databases.
- Run the installer for the Analytics database.

### Installing main ObserveIT databases

The commands below will start the ObserveIT database installer and will follow the installer log.

```PowerShell
Start-Process "C:\Temp\ObserveITx_Setup_vx.x.x.x\DB\SQLPackage.exe"
do {Start-Sleep -Seconds 5} while (!(Test-Path "C:\Temp\ObserveITx_Setup_vx.x.x.x\DB\SQL_Setup.txt"))
Get-Content "C:\Temp\ObserveITx_Setup_vx.x.x.x\DB\SQL_Setup.txt" -Wait -Last 10
```

### Installing ObserveIT Analytics database

The commands below will start the ObserveIT Analytics database installer and will follow the installer log.

```PowerShell
Start-Process "C:\Temp\ObserveITx_Setup_vx.x.x.x\DB_Analytics\SQLPackage.exe"
do {Start-Sleep -Seconds 5} while (!(Test-Path "C:\Temp\ObserveITx_Setup_vx.x.x.x\DB_Analytics\SQL_Setup.txt"))
Get-Content "C:\Temp\ObserveITx_Setup_vx.x.x.x\DB_Analytics\SQL_Setup.txt" -Wait -Last 10
```

### Moving ObserveIT databases to another drive

If you need to move ObserveIT databases to another (non-C:) drive, see [Moving ObserveIT databases to another drive](https://bitbucket.org/jonathan-boyko-oit/oit-powershell-semiautomated/src/master/Moving%20database%20files%20to%20another%20drive.md).

## Installing ObserveIT components

### Installing ObserveIT Application Server

If using Active Directory Service Account to run ObserveIT services, start elevated PowerShell as the Service Account first (described in the "Running PowerShell as ObserveIT Service Account" section above).

#### Installing Application Server prerequisites

Execute the below command to ensure all necessary Windows Server prerequisites are installed:

```PowerShell
Install-WindowsFeature Web-Server, Web-WebServer, Web-Common-Http, Web-Default-Doc, Web-Dir-Browsing, Web-Http-Errors, Web-Static-Content, Web-Stat-Compression, Web-Security, Web-Filtering, Web-App-Dev, Web-Net-Ext45, Web-Asp, Web-Asp-Net45, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Mgmt-Tools, Web-Mgmt-Console, NET-WCF-Services45, NET-WCF-HTTP-Activation45 –IncludeManagementTools
```

#### Installing new Application Server

Follow the commands in the section below when installing a new ObserveIT Application Server. Skip the sections below when upgrading a current Application Server and proceed to "Install Application Server prerequisites" section below.

##### Create an IIS website for the Application Server

Execute the commands below to create file system folders and Microsoft IIS infrastructure required for ObserveIT. This will also remove the **Default Web Site** installed automatically by Microsoft IIS.

```PowerShell
New-Item 'C:\Program Files\ObserveIT' -Type Directory
New-Item 'C:\Program Files\ObserveIT\Web' -Type Directory
Import-Module WebAdministration
New-Item IIS:\AppPools\ObserveITApplication
New-Item IIS:\Sites\ObserveITApplication -PhysicalPath 'C:\Program Files\ObserveIT\Web\' -Bindings @{protocol="https";bindingInformation=":yyy:"}
Set-ItemProperty IIS:\Sites\ObserveITApplication\ -Name applicationpool -Value ObserveITApplication
Get-Item 'IIS:\Sites\Default Web Site\' | Remove-Item -Recurse
Get-Item 'IIS:\AppPools\DefaultAppPool\' | Remove-Item -Recurse
```

##### Disable IIS logging for the ObserveIT Web Management Console website

```PowerShell
Set-ItemProperty -Path "IIS:\Sites\ObserveITApplication" -Name Logfile.enabled -Value $false
```

##### Configuring Application Pool recycling settings for ObserveIT Application Server

It is a best practice to recycle the Application Pool every 8 hours to make sure the application is healthy. Use the commands below to configure recommended recycling settings.

```PowerShell
Import-Module WebAdministration
$AppPoolPath = "IIS:\AppPools\ObserveITApplication"
# Set the Application Pool to recycle every 8 hours
Set-ItemProperty $AppPoolPath -Name Recycling.periodicRestart.time -Value 0.08:00:00
Clear-ItemProperty $AppPoolPath -Name Recycling.periodicRestart.schedule
$RestartAt = @('12:00', '20:00', '07:00')
# Set recycling to occur at 12pm, 8pm, and 7am, when there's less load on the system
New-ItemProperty -Path $AppPoolPath -Name Recycling.periodicRestart.schedule -Value $RestartAt
```

##### Create a self-signed certificate

If the current customer environment has no certificate infrastructure - such as an Active Directory Certificate Authority - you can generate a self-signed certificate for the Application Server. Execute the commands below to generate a self-signed certificate and assign it to the Application Server website.

**NOTE**: While the self-signed certificate commands below are split into sections, the intent is for you to execute them one after the other in succession. Otherwise, command execution will fail.

###### Generate the self-signed certificate

```PowerShell
$HostName = ([System.Net.Dns]::GetHostByName(($env:computerName))).HostName
$SelfSignedCertificate = New-SelfSignedCertificate -CertStoreLocation Cert:\LocalMachine\My\ -DnsName $HostName
```

###### Assign the self-signed certificate to the Application Server website

```PowerShell
$binding = Get-WebBinding -Name 'ObserveITApplication' -Protocol https
$binding.AddSslCertificate($SelfSignedCertificate.GetCertHashString(), 'my')
```

###### Export the self-signed certificate

The command below exports the self-signed certificate to disk. This is useful when you need to import the self-signed certificate into another machine to establish trust between the two machines.

```PowerShell
Export-Certificate -Cert $SelfSignedCertificate -FilePath "C:\Temp\$HostName.cer" -Force
```

#### Require SSL for the Application Server website

When using https with the Application Server website, use the commands below to require secure connection when connecting to the Application Server website.

>NOTE: Close the IIS Management Console before running the commands. IISMC locks the configuration file, and the commands below will produce an error.

```PowerShell
$ConfigSection = Get-IISConfigSection -SectionPath "system.webServer/security/access" -Location "ObserveITApplication"
Set-IISConfigAttributeValue -AttributeName sslFlags -AttributeValue Ssl -ConfigElement $ConfigSection
Get-IISConfigAttributeValue -ConfigElement $ConfigSection -AttributeName sslFlags
```

#### Install Application Server prerequisites

Run the command below to install or upgrade Application Server prerequisites. Execute this command both when installing or upgrading:

```PowerShell
Start-Process "C:\Temp\ObserveITx_Setup_vx.x.x.x\Web\PreRequisite_nodeServices.exe" -ArgumentList "common=1", "/install", "/quiet", "/norestart", "/log PreRequisite_nodeService.log" -Wait
```

#### Install the Application Server component

After installing the prerequisites, run the command below to install the ObserveIT Application Server component. The command below will also follow the installation log. When done, press CTRL+C to stop following the installation log.

```PowerShell
iisreset /start
Start-Process msiexec -ArgumentList '/i', "C:\Temp\ObserveITx_Setup_vx.x.x.x\Web\AppServer\ObserveIT.AppServerSetup.msi", '/norestart', '/l*v ObserveITAppServer_setup.txt'
do {Start-Sleep -Seconds 5} while (!(Test-Path $env:TEMP\"AppServer_CA_Log.txt"))
Get-Content $env:TEMP\"AppServer_CA_Log.txt" -Wait -Last 10
```

#### Configure correct Application server URLs

Here we update URLs of the Application servers in the ObserveIT database to use FQDN instead of just hostname. Run this query first to get a list of Application servers after installing the last Application server:

```SQL
SELECT * FROM ObserveIT.dbo.ApplicationServers
```

For each Application server (each row in the table), execute the following query:

```SQL
UPDATE ObserveIT.dbo.ApplicationServers SET ApplicationServerUrl = 'https://<# load balancer FQDN #>/ObserveITApplicationServer/' WHERE ServerName = '<# Application server hostname #>'
UPDATE ObserveIT.dbo.ApplicationServers SET ApplicationServerInternalUrl = 'https://<# Application server FQDN #>/ObserveITApplicationServer/' WHERE ServerName = '<# Application server hostname #>'
```

#### Test ObserveIT Application Server is online and operational

Execute the commands below to validate the current Application Server is operational.

##### Disable TLS trust

Since the command to test the current Application Server connects to the **localhost** address, the TLS trust will fail by default. Execute the command below to disable the TLS trust for the current session.

```PowerShell
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
```

##### Test the Application Server

Execute the PowerShell command below to test the Application Server.

```PowerShell
iisreset /start
Import-Module WebAdministration
Get-IISSite ObserveITApplication | Start-IISSite
Invoke-WebRequest 'https://localhost:yyy/ObserveitApplicationServer/v2/apis/health/_health' -UseBasicParsing
```

You should receive a response similar to this:

```Text
{"_status":{"status":200,"code":"it:error:none"},"_meta":{"origin":{},"stats":{}},"status":"Healthy","database":{"status":"Healthy","roundTripTime":"00:00:00.0028587"},"fileSystem":{"status":"Healthy","roundTripTime":"00:00:00.0055812"}}
```

### Installing ObserveIT Console

If using Active Directory Service Account to run ObserveIT services, start elevated PowerShell as the Service Account first (described in the "Running PowerShell as ObserveIT Service Account" section above).

The commands below assume you are installing ObserveIT Console on a separate machine. If you are installing the ObserveIT Console on a server with other ObserveIT components - like the Application Server - you will need to change the port assigned to the ObserveIT Console website (zzz by default). The suggested port is 8zzz.

#### Installing ObserveIT Console prerequisites

Execute the below command to ensure all necessary Windows Server prerequisites are installed:

```PowerShell
Install-WindowsFeature Web-Server, Web-WebServer, Web-Common-Http, Web-Default-Doc, Web-Dir-Browsing, Web-Http-Errors, Web-Static-Content, Web-Stat-Compression, Web-Security, Web-Filtering, Web-App-Dev, Web-Net-Ext45, Web-Asp, Web-Asp-Net45, Web-ISAPI-Ext, Web-ISAPI-Filter, Web-Mgmt-Tools, Web-Mgmt-Console, NET-WCF-Services45, NET-WCF-HTTP-Activation45 –IncludeManagementTools
```

##### Create an IIS website for the ObserveIT Console

Execute the commands below to create file system folders and Microsoft IIS infrastructure required for ObserveIT. This will also remove the **Default Web Site** installed automatically by Microsoft IIS.

```PowerShell
New-Item 'C:\Program Files\ObserveIT' -Type Directory
New-Item 'C:\Program Files\ObserveIT\Web' -Type Directory
Import-Module WebAdministration
New-Item IIS:\AppPools\ObserveITWebConsole
New-Item IIS:\Sites\ObserveITWebConsole -PhysicalPath 'C:\Program Files\ObserveIT\Web\' -Bindings @{protocol="https";bindingInformation=":zzz:"}
Set-ItemProperty IIS:\Sites\ObserveITWebConsole\ -Name applicationpool -Value ObserveITWebConsole
Get-Item 'IIS:\Sites\Default Web Site\' | Remove-Item -Recurse
Get-Item 'IIS:\AppPools\DefaultAppPool\' | Remove-Item -Recurse
```

##### Disable IIS logging for the ObserveIT Application Server website

```PowerShell
Set-ItemProperty -Path "IIS:\Sites\ObserveITWebConsole" -Name Logfile.enabled -Value $false
```

##### Configuring Application Pool recycling settings for ObserveIT Web Management Console

It is a best practice to recycle the Application Pool every 8 hours to make sure the application is healthy. Use the commands below to configure recommended recycling settings.

```PowerShell
Import-Module WebAdministration
$AppPoolPath = "IIS:\AppPools\ObserveITWebConsole"
# Set the Application Pool to recycle every 8 hours
Set-ItemProperty $AppPoolPath -Name Recycling.periodicRestart.time -Value 0.08:00:00
Clear-ItemProperty $AppPoolPath -Name Recycling.periodicRestart.schedule
$RestartAt = @('12:00', '20:00', '07:00')
# Set recycling to occur at 12pm, 8pm, and 7am, when there's less load on the system
New-ItemProperty -Path $AppPoolPath -Name Recycling.periodicRestart.schedule -Value $RestartAt
```

##### Create a self-signed certificate for ObserveIT management console

If the current customer environment has no certificate infrastructure - such as an Active Directory Certificate Authority - you can generate a self-signed certificate for the ObserveIT Console. Execute the commands below to generate a self-signed certificate and assign it to the ObserveIT Console website.

**NOTE**: While the self-signed certificate commands below are split into sections, the intent is for you to execute them one after the other in succession. Otherwise, command execution will fail.

###### Generate the self-signed certificate for ObserveIT management console

```PowerShell
$HostName = ([System.Net.Dns]::GetHostByName(($env:computerName))).HostName
$SelfSignedCertificate = New-SelfSignedCertificate -CertStoreLocation Cert:\LocalMachine\My\ -DnsName $HostName
```

###### Assign the self-signed certificate to the ObserveIT Console website

```PowerShell
$binding = Get-WebBinding -Name 'ObserveITWebConsole' -Protocol https
$binding.AddSslCertificate($SelfSignedCertificate.GetCertHashString(), 'my')
```

###### Export the self-signed certificate for ObserveIT management console

The command below exports the self-signed certificate to disk. This is useful when you need to import the self-signed certificate into another machine to establish trust between the two machines.

```PowerShell
Export-Certificate -Cert $SelfSignedCertificate -FilePath "C:\Temp\$HostName.cer" -Force
```

#### Require SSL for the ObserveIT Console website

When using https with the ObserveIT Console website, use the commands below to require secure connection when connecting to the ObserveIT Console website.

>NOTE: Close the IIS Management Console before running the commands. IISMC locks the configuration file, and the commands below will produce an error.

```PowerShell
$ConfigSection = Get-IISConfigSection -SectionPath "system.webServer/security/access" -Location "ObserveITWebConsole"
Set-IISConfigAttributeValue -AttributeName sslFlags -AttributeValue Ssl -ConfigElement $ConfigSection
Get-IISConfigAttributeValue -ConfigElement $ConfigSection -AttributeName sslFlags
```

#### Install ObserveIT Console prerequisites

Run the command below to install or upgrade ObserveIT Console prerequisites. Execute this command both when installing or upgrading:

```PowerShell
Start-Process "C:\Temp\ObserveITx_Setup_vx.x.x.x\Web\PreRequisite_nodeServices.exe" -ArgumentList "wconly=1", "sqlcli=1", "/install", "/quiet", "/norestart" ,"/log PreRequisite_nodeServices.log" -Wait
Start-Process msiexec -ArgumentList '/i', "C:\Temp\ObserveITx_Setup_vx.x.x.x\Web\sqlncli-2012-64-QFE.msi", "/quiet", "/norestart" -Wait
```

#### Install the ObserveIT Console component

After installing the prerequisites, run the command below to install the ObserveIT ObserveIT Console component. The command below will also follow the installation log. When done, press CTRL+C to stop following the installation log.

```PowerShell
iisreset /start
Start-Process msiexec -ArgumentList '/i', "C:\Temp\ObserveITx_Setup_vx.x.x.x\Web\WebConsole\ObserveIT.WebConsoleSetup.msi", '/norestart', 'EXTRACTMICROSERVICES=True', '/l*v ObserveITWebConsole_setup.txt'
do {Start-Sleep -Seconds 5} while (!(Test-Path $env:TEMP\"WebConsole_CA_Log.txt"))
Get-Content $env:TEMP\"WebConsole_CA_Log.txt" -Wait -Last 10
```

#### Extract the Advanced ObserveIT Web Management Console components

The commands below extract the ObserveIT APIs and the Developer Portal.

```PowerShell
<# Extract the Advanced ObserveIT Web Management Console #>
Set-Location "C:\Program Files\ObserveIT\Web\V2\apis"
Get-ChildItem *.zip | foreach {Expand-Archive -LiteralPath $_.FullName -DestinationPath $($_.Directory.ToString() + '\' + $_.BaseName.ToString()) -Force}
Set-Location "C:\Program Files\ObserveIT\Web\V2\apps"
Get-ChildItem *.zip | foreach {Expand-Archive -LiteralPath $_.FullName -DestinationPath $($_.Directory.ToString() + '\' + $_.BaseName.ToString()) -Force}
```

#### Test ObserveIT ObserveIT Console is online and operational

Execute the commands below to validate the current ObserveIT Console is operational.

##### Disable TLS trust for ObserveIT management console test

Since the command to test the current ObserveIT Console connects to the **localhost** address, the TLS trust will fail by default. Execute the command below to disable the TLS trust for the current session.

```PowerShell
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
```

##### Test the ObserveIT Console

Execute the PowerShell command below to test the ObserveIT Console.

```PowerShell
iisreset /start
Import-Module WebAdministration
Get-IISSite ObserveITWebConsole | Start-IISSite
(Invoke-WebRequest 'https://localhost:zzz/ObserveIT/FormLoginAuth.aspx?UserDefaultPage=True' -UseBasicParsing).RawContent -match 'ObserveIT - Login Page'
```

In the shell output, you should receive the result **True**.

##### Update the URL of the Application server

Next step is updating URLs of the load balancer pointint at the Application server (or just the FQDN of the Application server if using a single Application server).

Execute the SQL Server query below to do so. Best approach is using the SQL Management Studio. Don't forget to update the query below with the URL of the Application server.

```SQL
-- Update the load balancer URL
UPDATE ObserveIT.dbo.DatabaseConfiguration
SET DatabaseConfigurationValue = '<# http/https://<FQDN>/<# port #>/ObserveITApplicationServer #>'
WHERE DatabaseConfigurationKey = 'AppServerUrl'
GO
```

Next, for each Application server, update both internal and external addresses of the current Application server. The external URL of the Application server is the load balancer address (or address of a single Application server if not using a load balancer).

The internal URL is the one Health Monitoring service from the ObserveIT management console queries to determine Application server's health.

Use the queries below to update each Application server's address. Remember that both queries need to be executed for **each** Application server, meaning if you have 3 Application servers you will execute 6 queries total.

```SQL
-- Update addresses of each specific Application server
UPDATE ObserveIT.dbo.ApplicationServers
SET ApplicationServerUrl = '<# http/https://<FQDN of load balancer>/<# port #>/ObserveITApplicationServer #>'
WHERE ServerName = '<# hostname of this Application server #>'
GO

UPDATE ObserveIT.dbo.ApplicationServers
SET ApplicationServerInternalUrl = '<# http/https://<FQDN of this Application server>/<# port #>/ObserveITApplicationServer #>'
WHERE ServerName = '<# hostname of this Application server #>'
GO
```

Restart IIS now.

### Installing the ObserveIT Website Categorization module

If using Active Directory Service Account to run ObserveIT services, start elevated PowerShell as the Service Account first (described in the "Running PowerShell as ObserveIT Service Account" section above).

#### Install the ObserveIT Website Categorization module component

```PowerShell
Start-Process msiexec -ArgumentList '/i', "C:\Temp\ObserveITx_Setup_vx.x.x.x\WebsiteCat\Websitecat_Setup.msi", '/norestart', '/l*v ObserveITWebCat_setup.txt'
do {Start-Sleep -Seconds 5} while (!(Test-Path $env:TEMP\"WebsiteCat_CA_Log.txt"))
Get-Content $env:TEMP\"WebsiteCat_CA_Log.txt" -Wait -Last 10
```

#### Test connectivity to NetSTAR

Use the command below to validate the current server connects to the categories service provider - NetSTAR - correctly.

```PowerShell
(Invoke-WebRequest 'https://nsv10.netstar-inc.com/gcfus/get.cgi' -UseBasicParsing).Content
```

In your shell, expect the command to return the result **100**.

#### Configure the Website Categorization module proxy

If the current Website Categorization module deployment requires a connection through a proxy server, use the command below to open the configuration file. Configure the proxy settings under **PROXY_HOST** and **PROXY_PORT**.

Note that proxy authentication is not currently supported.

```PowerShell
notepad.exe 'C:\Program Files\ObserveIT\WebsiteCat\Adapters\NetStar\db\etc\gcf1.conf'
```

#### Run the Website Categorization module database download manually

When installing a Website Categorization module, the categories database download will begin automatically. When configuring a proxy server, or after troubleshooting, execute the command below to manually trigger the database download.

```PowerShell
&'C:\Program Files\ObserveIT\WebsiteCat\WebsiteCat.Manager.exe' -dw
```

#### Validate Website Categorization module resolves categories correctly

Use the command below to query the ObserveIT Website Categorization module for a website category.

```PowerShell
&'C:\Program Files\ObserveIT\WebsiteCat\WebsiteCat.Manager.exe' -URL google.com | Select-Object -Last 1
```

Expect the following result:

```Text
[I] Got category "Search Engines & Portals" (oitId=31) for the url "google.com"
```

#### Validate Application Servers resolve website categories correctly

ObserveIT Application Servers's Rule Engine Service uses the ObserveIT Website Categorization module to resolve website domains to a category. After successfully validating the Website Categorization module resolves a domain to a category, perform the following test to validate the Application Servers correctly reach the Website Categorization module and receive a response containing a category for a domain.

##### Disable TLS trust for Website Categorization module test

Since the command to test the current ObserveIT Console connects to the **localhost** address, the TLS trust will fail by default. Execute the command below to disable the TLS trust for the current session.

```PowerShell
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy
```

##### Test the category resolution

```PowerShell
(Invoke-WebRequest 'https://localhost:yyy/ObserveitApplicationServer/v2/apis/webcat/sites?url=google.com' -UseBasicParsing).Content
```

Expect a result similar to this:

```Text
{"_status":{"status":200,"code":"it:error:none"},"kind":"it:webcat:site","primary":{"name":"Search Engines & Portals","url":"google.com","groupName":"Search Engines & Portals","groupId":31,"kind":"it:webcat:category","id":0,"createdAt":"2020-05-03T16:02:57","updatedAt":"2020-05-04T13:46:30","createdBy":{"name":"webcat-service"}},"id":0,"site":"google.com","createdAt":"2020-05-03T16:02:57","url":"google.com","updatedAt":"2020-05-04T13:46:30","createdBy":{"name":"webcat-service"}}
```

### Installing the Install Screenshot Storage Optimizer

The ObserveIT Screenshot Storage Optimizer is the component moving the screen capture data from the Hot Storage to the Warm Storage. When using Hot Storage/Warm Storage configuration, Screenshot Storage Optimizer is essential for data movement. Most often, the ObserveIT Screenshot Storage Optimizer will be installed on the file server hosting the ObserveIT screen capture data.

Use the command below so initiate the Screenshot Storage Optimizer installation. If using Active Directory Service Account to run ObserveIT services, start elevated PowerShell as the Service Account first (described in the "Running PowerShell as ObserveIT Service Account" section above).


```PowerShell
Start-Process msiexec -ArgumentList '/i', "C:\Temp\ObserveITx_Setup_vx.x.x.x\ScreenshotsStorageOptimizer\ScreenshotsStorageOptimizer.msi", '/norestart', '/l*v ObserveITWebConsole_setup.txt'
do {Start-Sleep -Seconds 5} while (!(Test-Path $env:TEMP\"ScreenshotsStorageOptimizer_CA_Log.txt"))
Get-Content $env:TEMP\"ScreenshotsStorageOptimizer_CA_Log.txt" -Wait -Last 10
```

## Additional helpers

The additional scripts below are intended to help you when deploying ObserveIT, in particular, when the connection to the database is initialized using the ObserveIT built-in SQL login, rather than an Active Directory Service Account. 

In this configuration, the ObserveIT components will be configured to connect to the database using System Authentication, rather than a Service Account. To allow ObserveIT components connection to the file server storing the screen capture data, the ObserveIT services and IIS Application Pools need to be running under an Active Directory Service Account.

The commands below, will allow you to configure ObserveIT components to run as an Active Directory Service Account, without you having to configure all the components manually.

### Assign the Logon as a Service right to the ObserveIT Service Account

Execute the command below to assign the **Logon as a Service** right to the ObserveIT Active Directory Service Account. Without this right, the Active Directory account will not be able to start ObserveIT services.

In the last row of the command, make sure to replace the **'DOMAIN\account'** with the correct Service Account domain and username.

```Powershell
function Set-LogonRight ($accountToAdd) {
    if ( [string]::IsNullOrEmpty($accountToAdd) ) {
        Write-Output "no account specified"
        exit
}
    $sidstr = $null
try {
        $ntprincipal = new-object System.Security.Principal.NTAccount "$accountToAdd"
        $sid = $ntprincipal.Translate([System.Security.Principal.SecurityIdentifier])
        $sidstr = $sid.Value.ToString()
} catch {
        $sidstr = $null
}
    Write-Output "Account: $($accountToAdd)"
    if ( [string]::IsNullOrEmpty($sidstr) ) {
        Write-Output "Account not found!"
        exit -1
}
    Write-Output "Account SID: $($sidstr)"
    $tmp = [System.IO.Path]::GetTempFileName()
    Write-Output "Export current Local Security Policy"
    secedit.exe /export /cfg "$($tmp)" 
    $c = Get-Content -Path $tmp 
    $currentSetting = ""
    foreach ($s in $c) {
        if ( $s -like "SeServiceLogonRight*") {
            $x = $s.split("=", [System.StringSplitOptions]::RemoveEmptyEntries)
            $currentSetting = $x[1].Trim()
    }
}
    if ( $currentSetting -notlike "*$($sidstr)*" ) {
        Write-Output "Modify Setting ""Logon as a Service"""
        if ( [string]::IsNullOrEmpty($currentSetting) ) {
            $currentSetting = "*$($sidstr)"
    } else {
            $currentSetting = "*$($sidstr),$($currentSetting)"
    }
        Write-Output "$currentSetting"
$outfile = @"
[Unicode]
Unicode=yes
[Version]
signature="`$CHICAGO`$"
Revision=1
[Privilege Rights]
SeServiceLogonRight = $($currentSetting)
"@
        $tmp2 = [System.IO.Path]::GetTempFileName()
        Write-Output "Import new settings to Local Security Policy"
        $outfile | Set-Content -Path $tmp2 -Encoding Unicode -Force
        Push-Location (Split-Path $tmp2)
    try {
            secedit.exe /configure /db "secedit.sdb" /cfg "$($tmp2)" /areas USER_RIGHTS 
    } finally {	
            Pop-Location
    }
} else {
        Write-Output "NO ACTIONS REQUIRED! Account already in ""Logon as a Service"""
}
    Write-Output "Done."
}

Set-LogonRight 'DOMAIN\account'
```

### Set ObserveIT Service Account as the account for all ObserveIT components on the machine

Execute the command below to assign the ObserveIT Active Directory Service Account to all ObserveIT components, including ObserveIT services and Microsoft IIS Application Pools.

```Powershell
function Set-OITAccount ($Credentials){
    if (!$Credentials) {
        $Credentials = Get-Credential
    }
    $UserName = $Credentials.GetNetworkCredential().UserName + '@' + $Credentials.GetNetworkCredential().Domain
    $Password = $Credentials.GetNetworkCredential().Password

    $OITServices = Get-Service observeit*, screenshot*, websitecat*, gcf1* | Where-Object {$_.DisplayName -notlike "*Controller*"}
    foreach ($Service in $OITServices) {
        $Service = $Service.Name
        Write-Output "Working service $Service"
        $svc_Obj = Get-WmiObject Win32_Service -filter "name='$service'"
        $ChangeStatus = $svc_Obj.change($null, $null, $null, $null, $null,
            $null, $UserName, $Password, $null, $null, $null)
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "User Name sucessfully changed for the service '$Service'"}
        If ($ChangeStatus.ReturnValue -eq "0")  
        {Write-host "The service '$Service' Started successfully"}
    }

    foreach ($service in $OITServices) {
        Get-Service $Service.Name | Restart-Service -Force
    }
    Write-Output "Setting credentials for the ObserveIT Application Pool"
    Import-Module WebAdministration
    Get-Item IIS:\AppPools\ObserveIT* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRepository* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Get-Item IIS:\AppPools\AppPoolRegistry* | Set-ItemProperty -name processModel -value @{userName = "$UserName"; password = "$Password"; identitytype = 3}
    Start-Process iisreset -NoNewWindow
}

Set-OITAccount
```
