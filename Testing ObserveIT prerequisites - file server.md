# Testing ObserveIT prerequisites - file server

Follow the steps bellow to test for ObserveIT prerequisites on the file server hosting the ObserveIT screen capture data. As a reminder, note that the commands above should be pasted into the elevated Microsoft PowerShell window.

## The ObserveIT storage is formatted with correct block sizes

The ObserveIT Hot Storage should be formatted with 4 KB block size, while the Warm Storage and the Cold Storage should be formatted with 64 KB block sizes. The command above will output block size for each drive on the local machine.

```PowerShell
Get-WmiObject -Class Win32_Volume | Select-Object Name, Label, BlockSize | Format-Table -AutoSize
```

Expect result similar to this:

| Name | Label | BlockSize |
| ---- | ----- | --------- |
| C:\  |       | 4096      |
| D:\  |       | 4096      |
| E:\  |       | 65536     |
| F:\  |       | 65536     |

## The ObserveIT screen capture data volumes are shared

This step validates that ObserveIT screen capture data folders are shared on the local machine. The command will output all shares on the local machine, except the system shares.

```PowerShell
Get-SmbShare | Where-Object {$_.Description -ne "Default share" -and $_.Description -ne "Remote Admin" -and $_.Description -ne "Remote IPC"}
```

Expect a result similar to below. Validate that there's a share for ObserveIT Hot Storage, Warm Storage, and Cold Storage.

| Name        | ScopeName | Path           | Description |
| ----------- | --------- | -------------- | ----------- |
| ColdStorage | *         | F:\ColdStorage |             |
| HotStorage  | *         | D:\HotStorage  |             |
| WarmStorage | *         | E:\WarmStorage |             |

## ObserveIT Active Directory Service Account has permissions to access the shares

This step validates the ObserveIT Active Directory Service Account has necessary permissions to access the shared folders for the Hot Storage, Warm Storage, and the Cold Storage over the network.

```PowerShell
Get-SmbShare | Where-Object {$_.Description -ne "Default share" -and $_.Description -ne "Remote Admin" -and $_.Description -ne "Remote IPC"} | Get-SmbShareAccess
```

Make sure the ObserveIT Service Account (noted as **OITServiceAccount** in this example) has at least **Change** privileges on the share.

| Name         | ScopeName | AccountName                | AccessControlType | AccessRight |
| ------------ | --------- | -------------------------- | ----------------- | ----------- |
| HotStorage   | *         | Everyone                   | Allow             | Change      |
| HotStorage2  | *         | mydomain\OITServiceAccount | Allow             | Change      |
| ColdStorage2 | *         | mydomain\OITServiceAccount | Allow             | Change      |
| ColdStorage  | *         | Everyone                   | Allow             | Change      |
| WarmStorage  | *         | Everyone                   | Allow             | Change      |
| WarmStorage2 | *         | mydomain\OITServiceAccount | Allow             | Change      |

## ObserveIT Active Directory Service Account has folders

This step validates the ObserveIT Active Directory Service Account has necessary permissions to access the folders on the local volume. Together with share access over the network, this would ensure ObserveIT can read from and write to the folders for ObserveIT screen capture data.

```PowerShell
Get-SmbShare | Where-Object {$_.Description -ne "Default share" -and $_.Description -ne "Remote Admin" -and $_.Description -ne "Remote IPC"} | Get-ACL
```

Exect a result similar to below. Please note the bold text, denoting the folder name, account accessing, and the permission.

In the result, you must have either the **Everyone** account or specifically the ObserveIT Service Account mentioned.

The permissions must be either **Modify** or **FullControl**.

```Text
Path   : Microsoft.PowerShell.Core\FileSystem::D:\HotStorage

Owner  : BUILTIN\Administrators

Group  : EC2AMAZ-US92KRD\None

Access : 

Everyone Allow  Modify, Synchronize

BUILTIN\Administrators Allow  FullControl

BUILTIN\Administrators Allow  FullControl

mydomain\OITServiceAccount Allow Modify

NT AUTHORITY\SYSTEM Allow  FullControl

CREATOR OWNER Allow  268435456

BUILTIN\Users Allow  ReadAndExecute, 
Synchronize

BUILTIN\Users Allow  AppendData

BUILTIN\Users Allow  CreateFiles

Audit  :

Sddl   : 

O:BAG:S-1-5-21-184946049-2594445221-2459348775-513D:AI(A;OICI;0x1301bf;;;WD)(A;;FA;;;BA)(A;OICIID;FA;;;BA)(A;OICIID;FA;;;SY)(A;OICIIOID;GA;;;CO)(A;OICIID;0x1200a9;;;BU)(A;CIID;LC;
         ;;BU)(A;CIID;DC;;;BU)
```
